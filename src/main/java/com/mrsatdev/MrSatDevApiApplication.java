package com.mrsatdev;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RestController;

@RestController
@SpringBootApplication
public class MrSatDevApiApplication {
    public static void main(String[] args) {
        SpringApplication.run(MrSatDevApiApplication.class, args);
    }
}