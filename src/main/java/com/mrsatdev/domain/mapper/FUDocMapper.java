package com.mrsatdev.domain.mapper;

import com.mrsatdev.application.dto.response.fu_doc.CourseDetailResponse;
import com.mrsatdev.application.dto.response.fu_doc.CourseItemResponse;
import com.mrsatdev.application.dto.response.fu_doc.DocumentDetailResponse;
import com.mrsatdev.infrastructure.entity.model.FUCourse;
import com.mrsatdev.infrastructure.entity.model.FUDocument;
import org.modelmapper.ModelMapper;

public class FUDocMapper {

    public static CourseDetailResponse toCourseDetailResponse(ModelMapper modelMapper, FUCourse source) {
        return modelMapper.map(source, CourseDetailResponse.class);
    }

    public static CourseItemResponse toCourseItemResponse(ModelMapper modelMapper, FUCourse source) {
        return modelMapper.map(source, CourseItemResponse.class);
    }

    public static DocumentDetailResponse toDocumentDetailResponse(ModelMapper modelMapper, FUDocument source) {
        return modelMapper.map(source, DocumentDetailResponse.class);
    }
}
