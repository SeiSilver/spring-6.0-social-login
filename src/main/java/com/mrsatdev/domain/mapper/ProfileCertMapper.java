package com.mrsatdev.domain.mapper;

import com.mrsatdev.application.dto.response.profile_cert.ProfileCertDetailResponse;
import com.mrsatdev.application.dto.response.profile_cert.ProfileCertItemResponse;
import com.mrsatdev.infrastructure.entity.model.ProfileCert;
import org.modelmapper.ModelMapper;

public class ProfileCertMapper {

    public static ProfileCertDetailResponse toProfileCertDetailResponse(ModelMapper modelMapper, ProfileCert source) {
        return modelMapper.map(source, ProfileCertDetailResponse.class);
    }

    public static ProfileCertItemResponse toProfileCertItemResponse(ModelMapper modelMapper, ProfileCert source) {
        return modelMapper.map(source, ProfileCertItemResponse.class);
    }
}
