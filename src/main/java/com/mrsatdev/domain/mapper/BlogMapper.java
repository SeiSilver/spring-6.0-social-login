package com.mrsatdev.domain.mapper;

import com.mrsatdev.application.dto.response.blog.BlogItemResponse;
import com.mrsatdev.application.dto.response.blog.BlogManagedResponse;
import com.mrsatdev.application.dto.response.blog.BlogPublishedResponse;
import com.mrsatdev.infrastructure.entity.model.BlogData;
import org.modelmapper.ModelMapper;

public class BlogMapper {

    public static BlogItemResponse toBlogItemResponse(ModelMapper modelMapper, BlogData source) {
        return modelMapper.map(source, BlogItemResponse.class);
    }

    public static BlogManagedResponse toBlogManagedResponse(ModelMapper modelMapper, BlogData source) {
        return modelMapper.map(source, BlogManagedResponse.class);
    }

    public static BlogPublishedResponse toBlogPublishedResponse(ModelMapper modelMapper, BlogData source) {
        return modelMapper.map(source, BlogPublishedResponse.class);
    }
}
