package com.mrsatdev.domain.mapper;

import com.mrsatdev.application.dto.response.user.UserDetailProfileResponse;
import com.mrsatdev.application.dto.response.user.UserLoginResponse;
import com.mrsatdev.application.dto.response.user.UserManagedItemResponse;
import com.mrsatdev.application.dto.response.user.UserPublicProfileResponse;
import com.mrsatdev.infrastructure.entity.model.AuthUser;
import org.modelmapper.ModelMapper;

public class UserMapper {

    public static UserLoginResponse toUserLoginResponse(ModelMapper modelMapper, AuthUser source) {
        return modelMapper.map(source, UserLoginResponse.class);
    }

    public static UserManagedItemResponse toUserManagedItemResponse(ModelMapper modelMapper, AuthUser source) {
        return modelMapper.map(source, UserManagedItemResponse.class);
    }

    public static UserPublicProfileResponse toUserPublicProfileResponse(ModelMapper modelMapper, AuthUser source) {
        return modelMapper.map(source, UserPublicProfileResponse.class);
    }

    public static UserDetailProfileResponse toUserDetailProfileResponse(ModelMapper modelMapper, AuthUser source) {
        return modelMapper.map(source, UserDetailProfileResponse.class);
    }
}
