package com.mrsatdev.domain.mapper;

import com.mrsatdev.application.dto.response.contact.ContactDetailResponse;
import com.mrsatdev.application.dto.response.contact.ContactItemResponse;
import com.mrsatdev.application.dto.response.contact.ContactReplyResponse;
import com.mrsatdev.infrastructure.entity.model.ContactForm;
import org.modelmapper.ModelMapper;

public class ContactMapper {

    public static ContactItemResponse toContactItemResponse(ModelMapper modelMapper, ContactForm source) {
        return modelMapper.map(source, ContactItemResponse.class);
    }

    public static ContactDetailResponse toContactDetailResponse(ModelMapper modelMapper, ContactForm source) {
        return modelMapper.map(source, ContactDetailResponse.class);
    }

    public static ContactReplyResponse toContactReplyResponse(ModelMapper modelMapper, ContactForm source) {
        return modelMapper.map(source, ContactReplyResponse.class);
    }
}
