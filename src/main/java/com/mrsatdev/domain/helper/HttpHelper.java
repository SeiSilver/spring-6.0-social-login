package com.mrsatdev.domain.helper;

import com.mrsatdev.application.dto.DataListResponse;
import com.mrsatdev.application.dto.DataResponse;
import com.mrsatdev.application.dto.GenericResponse;

import com.mrsatdev.application.dto.common.MetaDataResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

public class HttpHelper {

    public static ResponseEntity<GenericResponse> createGenericOKResponse(HttpStatus httpStatus, String message) {
        GenericResponse genericResponse = GenericResponse.builder()
                .timeStamp(LocalDateTime.now())
                .httpStatus(httpStatus)
                .message(message)
                .errors(null)
                .build();
        return new ResponseEntity<>(genericResponse, httpStatus);
    }

    public static ResponseEntity<GenericResponse> createGenericErrorResponse(HttpStatus httpStatus, String errMessage, Map<String, ?> errors) {
        GenericResponse genericResponse = GenericResponse.builder()
                .timeStamp(LocalDateTime.now())
                .httpStatus(httpStatus)
                .message(errMessage)
                .errors(errors)
                .build();
        return new ResponseEntity<>(genericResponse, httpStatus);
    }

    public static ResponseEntity<DataResponse> createDataResponse(HttpStatus httpStatus, Object data, String message) {
        DataResponse res = DataResponse.builder()
                .timeStamp(LocalDateTime.now())
                .httpStatus(httpStatus)
                .data(data)
                .message(message)
                .build();
        return ResponseEntity.status(HttpStatus.OK).body(res);
    }

    public static ResponseEntity<DataListResponse> createDataListResponse(HttpStatus httpStatus, List<?> data, MetaDataResponse metaData, String message) {
        DataListResponse res = DataListResponse.builder()
                .timeStamp(LocalDateTime.now())
                .httpStatus(httpStatus)
                .data(data)
                .metaData(metaData)
                .message(message)
                .build();
        return ResponseEntity.status(HttpStatus.OK).body(res);
    }
}
