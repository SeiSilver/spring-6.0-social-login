package com.mrsatdev.domain.constant;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class DataResponseConstant {
    public static final String TIME_PATTERN = "dd-MM-yyyy hh:mm:ss";
    public static final String TIMEZONE = "Asia/Ho_Chi_Minh";
    public static final String GET_OK = "OK: Get data completed successfully";
    public static final String ACTION_OK = "OK: Action performed successfully";
}