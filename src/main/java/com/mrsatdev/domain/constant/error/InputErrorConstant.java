package com.mrsatdev.domain.constant.error;

public class InputErrorConstant {
    public static final String ERROR_IMG_TYPE = "Image type not accepted";
    public static final String BLANK_ARG = "This field is empty";
    public static final String BAD_USERNAME = "Username wrong format";
    public static final String BAD_PASSWORD = "Password wrong format";
    public static final String BAD_EMAIL = "Email wrong format";
    public static final String BAD_NAME = "Name wrong format";
    public static final String BAD_TEXT = "Your input was too long or contain unaccepted content";
    public static final String BAD_NUMBER = "Your input number in wrong format or out of range";
    public static final String BAD_PHONE = "Phone wrong format";
    public static final String BAD_DATE = "Date wrong format, please using YYYY-MM-DD pattern";
    public static final String BAD_DATETIME = "The time is in wrong format";
}
