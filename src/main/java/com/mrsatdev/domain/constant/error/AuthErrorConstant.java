package com.mrsatdev.domain.constant.error;

public class AuthErrorConstant {
    public static final String IDENTITY_ALREADY_EXISTS = "Email or username already exists: '%s'";
    public static final String INCORRECT_CREDENTIALS = "Username / password incorrect. Please try again";
    public static final String ACCOUNT_LOCKED = "Your account has been locked. Please contact administration";
    public static final String ERROR_GRANT = "Error occurred while grant permission";
    public static final String ERROR_OAUTH = "Oauth error or not supported";
    public static final String NOT_ENOUGH_PERMISSION = "You do not have enough permission";
}
