package com.mrsatdev.domain.constant.error;

public class CommonErrorConstant {
    public static final String METHOD_IS_NOT_ALLOWED = "Request method is not allowed!";
    public static final String BAD_REQUEST = "Bad request";
    public static final String INTERNAL_SERVER_ERROR = "An error occurred while processing the request";
    public static final String ERROR_PROCESSING_IMG = "Error occurred while processing image: %s";
    public static final String ERROR_PROCESSING_FILE = "Error occurred while processing file: %s";
    public static final String API_NOT_FOUND = "There is no API available (at least for you) with this endpoint";
    public static final String DATA_NOT_FOUND = "Sorry, but the resource you looking for not available";
    public static final String OBJECT_NOT_FOUND = "Object you are looking for was removed or not exits: %s";
}
