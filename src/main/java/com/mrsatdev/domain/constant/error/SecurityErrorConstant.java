package com.mrsatdev.domain.constant.error;

public class SecurityErrorConstant {
    public static final String ACCESS_DENIED_MESSAGE = "Access denied by System";
    public static final String FORBIDDEN_MESSAGE = "Forbidden";
    public static final String CSRF_ACCESS_DENIED_MESSAGE = "This request is blocked by System";
}
