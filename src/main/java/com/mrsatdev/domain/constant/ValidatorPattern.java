package com.mrsatdev.domain.constant;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ValidatorPattern {
    public static final String BLANK_PATTERN = "^\\s*$";
    public static final String NOT_BLANK_PATTERN = "(.|\\s)*\\S(.|\\s)*";
    public static final String SHORT_TEXT_PATTERN = "^[\\s\\S]{8,30}$";
    public static final String MEDIUM_TEXT_PATTERN = "^[\\s\\S]{30,300}$";
    public static final String LONG_TEXT_PATTERN = "^[\\s\\S]{255,10000}$";
    public static final String NORMAL_NAME_PATTERN = "^[^!@#$%^&*_+\\-=\\[\\]{};':\"\\\\|,.<>\\/?~]+$";
    public static final String USERNAME_PATTERN = "^(?=.{8,20}$)(?![_.])(?!.*[_.]{2})[a-zA-Z0-9._]+(?<![_.])$";
    public static final String EMAIL_PATTERN = "^[\\w-\\.]+@([\\w-]+\\.)+[\\w-]{2,4}$";
    public static final String PASSWORD_PATTERN = "^(?=.*[a-z])(?=.*[A-Z])(?=.*[@$!%*?&]?)(?=.*\\d)[a-zA-Z\\d@$!%*?&]{8,}$";
    public static final String PHONE_NUMBER_PATTERN = "([\\+84|84|0]+(3|5|7|8|9|1[2|6|8|9]))+([0-9]{8})\\b";
    public static final String DATE_PATTERN = "^(\\d{4})-(0[1-9]|1[0-2])-(0[1-9]|[12][0-9]|3[01])$";
}
