package com.mrsatdev.infrastructure.entity.model;

import io.hypersistence.utils.hibernate.id.Tsid;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import lombok.*;

import java.sql.Timestamp;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "blog_engagement")
public class BlogEngagement {

    @Id
    @Tsid
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "machine_ip", nullable = false)
    private String machineIP;

    @Column(name = "user_id")
    private String userid;

    @Column(name = "date_interact", nullable = false)
    private Timestamp dateInteract;

    @Column(name = "rating", columnDefinition = "double DEFAULT 2")
    private Double rating;

    @Column(name = "view_count", columnDefinition = "int DEFAULT 2")
    private Integer viewCount;
}
