package com.mrsatdev.infrastructure.entity.model;

import jakarta.persistence.*;
import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "csrf")
public class CSRFData {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "session", nullable = false)
    private Long session;

    @Column(name = "j_key", nullable = false)
    private String key;
}
