package com.mrsatdev.infrastructure.entity.model;

import com.mrsatdev.infrastructure.entity.common.BaseEntity;
import io.hypersistence.utils.hibernate.id.Tsid;
import jakarta.persistence.*;
import lombok.*;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import java.io.Serial;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;

@Getter
@Setter
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "fu_document")
@EntityListeners(AuditingEntityListener.class)
public class FUDocument extends BaseEntity implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    @Id
    @Tsid
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "title", nullable = false)
    private String title;

    @Column(name = "description", columnDefinition = "TEXT")
    private String description;

    @Column(name = "picture_url", nullable = false)
    private String pictureURL;

    @Column(name = "link", nullable = false)
    private String link;

    @Column(name = "code", nullable = false)
    private String code;

    @ElementCollection
    @Column(name = "change_description", columnDefinition = "TEXT")
    private List<String> changeDescription;

    @Column(name = "view", columnDefinition = "int DEFAULT 2")
    private Integer view;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FUDocument that = (FUDocument) o;
        return id != null && Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
