package com.mrsatdev.infrastructure.entity.model;

import com.mrsatdev.infrastructure.entity.common.BaseEntity;
import com.mrsatdev.infrastructure.entity.enumeration.ContactFormType;
import com.mrsatdev.infrastructure.entity.enumeration.ContactStatusType;
import io.hypersistence.utils.hibernate.id.Tsid;
import jakarta.persistence.*;
import lombok.*;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import java.io.Serial;
import java.io.Serializable;

@Getter
@Setter
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "contact")
@EntityListeners(AuditingEntityListener.class)
public class ContactForm extends BaseEntity implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    @Id
    @Tsid
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "parent_id")
    private Long parentId;

    @Column(name = "status", nullable = false)
    @Enumerated(EnumType.STRING)
    private ContactStatusType status;

    @Column(name = "type", nullable = false)
    @Enumerated(EnumType.STRING)
    private ContactFormType type;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user", nullable = false)
    @ToString.Exclude
    private AuthUser requester;

    @Column(name = "title", nullable = false)
    private String title;

    @Column(name = "detail", columnDefinition = "TEXT", nullable = false)
    private String detail;

    @Column(name = "contact_link", nullable = false)
    private String contactLink;
}
