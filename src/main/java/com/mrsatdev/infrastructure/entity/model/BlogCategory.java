package com.mrsatdev.infrastructure.entity.model;

import io.hypersistence.utils.hibernate.id.Tsid;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "blog_category")
public class BlogCategory {

    @Id
    @Tsid
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "tag", nullable = false)
    private String tag;

    @Column(name = "blog_count", columnDefinition = "int DEFAULT 0")
    private int blogCount;
}
