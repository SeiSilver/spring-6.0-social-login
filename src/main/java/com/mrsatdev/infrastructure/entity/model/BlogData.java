package com.mrsatdev.infrastructure.entity.model;

import com.mrsatdev.infrastructure.entity.common.BaseEntity;
import com.mrsatdev.infrastructure.entity.enumeration.BlogStatus;
import com.mrsatdev.infrastructure.entity.enumeration.BlogType;
import io.hypersistence.utils.hibernate.id.Tsid;
import jakarta.persistence.*;
import lombok.*;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import java.io.Serial;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;
import java.util.Objects;

@Getter
@Setter
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "blog_data")
@EntityListeners(AuditingEntityListener.class)
public class BlogData extends BaseEntity implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    @Id
    @Tsid
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "friendly_name", nullable = false, unique = true)
    private String friendlyName;

    @Column(name = "title", nullable = false)
    private String title;

    @Column(name = "description", columnDefinition = "TEXT", nullable = false)
    private String description;

    @Column(name = "picture_url", nullable = false)
    private String pictureUrl;

    @Column(name = "type", nullable = false)
    @Enumerated(EnumType.STRING)
    private BlogType type;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "writer", nullable = false)
    @ToString.Exclude
    private AuthUser writer;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "publisher", nullable = false)
    @ToString.Exclude
    private AuthUser publisher;

    @Column(name = "lst_publish_date", nullable = false)
    private Timestamp lstPublishDate;

    @Column(name = "status", nullable = false)
    @Enumerated(EnumType.STRING)
    private BlogStatus status;

    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinTable(name = "blog_data_category", joinColumns = @JoinColumn(name = "blog_id", nullable = false), inverseJoinColumns = @JoinColumn(name = "category_id", nullable = false))
    private List<BlogCategory> categories;

    @Column(name = "html", columnDefinition = "TEXT", nullable = false)
    private String html;

    @Column(name = "video_url")
    private String videoUrl;

    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumn(name = "blog_id", nullable = false)
    @ToString.Exclude
    private List<BlogEngagement> engagements;

    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumn(name = "blog_id", nullable = false)
    @ToString.Exclude
    private List<BlogComment> comments;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BlogData that = (BlogData) o;
        return id != null && Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
