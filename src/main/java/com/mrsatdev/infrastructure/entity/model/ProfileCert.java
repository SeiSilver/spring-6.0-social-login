package com.mrsatdev.infrastructure.entity.model;

import com.mrsatdev.infrastructure.entity.common.BaseEntity;
import com.mrsatdev.infrastructure.entity.enumeration.ProfileCertType;
import io.hypersistence.utils.hibernate.id.Tsid;
import jakarta.persistence.*;
import lombok.*;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDate;

@Getter
@Setter
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "me_cert")
@EntityListeners(AuditingEntityListener.class)
public class ProfileCert extends BaseEntity implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    @Id
    @Tsid
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "type", nullable = false)
    @Enumerated(EnumType.STRING)
    private ProfileCertType type;

    @Column(name = "code", nullable = false)
    private String code;

    @Column(name = "organization", nullable = false)
    private String organization;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "detail", columnDefinition = "TEXT")
    private String detail;

    @Column(name = "picture_url", nullable = false)
    private String pictureUrl;

    @Column(name = "pdf_url")
    private String pdfUrl;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "owner", nullable = false)
    @ToString.Exclude
    private AuthUser owner;

    @Column(name = "grant_at")
    private LocalDate grantAt;
}
