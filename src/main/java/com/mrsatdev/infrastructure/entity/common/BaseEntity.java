package com.mrsatdev.infrastructure.entity.common;

import com.mrsatdev.infrastructure.entity.enumeration.ActivationStatusType;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import java.sql.Timestamp;

@Getter
@Setter
@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
public class BaseEntity {

    @CreatedBy
    @Column(name = "created_by", columnDefinition = "VARCHAR(50) DEFAULT 'System'")
    private String createdBy;

    @CreatedDate
    @Column(name = "created_at", columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    private Timestamp createdAt;

    @LastModifiedBy
    @Column(name = "updated_by", columnDefinition = "VARCHAR(50) DEFAULT 'System'")
    private String updatedBy;

    @LastModifiedDate
    @Column(name = "updated_at", columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    private Timestamp updatedAt;

    @Enumerated(EnumType.STRING)
    @Column(name = "activation_status", columnDefinition = "VARCHAR(50) DEFAULT 'ACTIVE'")
    private ActivationStatusType activationStatus = ActivationStatusType.ACTIVE;
}