package com.mrsatdev.infrastructure.entity.enumeration;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum CloudinaryFolderType {
    BLOG("blog"),
    USER_AVATAR("user"),
    FU_DOCUMENT("fu_doc");

    private final String value;
}
