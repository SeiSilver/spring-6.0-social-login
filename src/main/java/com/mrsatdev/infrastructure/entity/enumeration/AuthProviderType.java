package com.mrsatdev.infrastructure.entity.enumeration;

public enum AuthProviderType {
    SYSTEM,
    LOCAL,
    FACEBOOK,
    GOOGLE
}
