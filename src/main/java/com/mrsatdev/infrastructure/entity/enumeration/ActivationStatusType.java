package com.mrsatdev.infrastructure.entity.enumeration;

public enum ActivationStatusType {
    ACTIVE,
    LOCKED,
    DELETED
}
