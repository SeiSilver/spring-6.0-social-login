package com.mrsatdev.infrastructure.entity.enumeration;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum BlogStatus {
    DRAFT("Just created"),
    SUMMITED("Submitted for review"),
    PUBLISHED("Live, and can view by anyone"),
    PRIVATE("Live, can view content, but not visible on website"),
    LOCKED("Live, but content is locked, not visible on website");

    private final String description;
}
