package com.mrsatdev.infrastructure.entity.enumeration;

public enum ContactStatusType {
    OPEN,
    RESOLVE
}
