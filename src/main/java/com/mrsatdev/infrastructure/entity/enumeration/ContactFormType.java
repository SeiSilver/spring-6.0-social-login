package com.mrsatdev.infrastructure.entity.enumeration;

public enum ContactFormType {
    REPORT_BUG,
    SUPPORT,
    FOR_WORK,
    REPLY,
}
