package com.mrsatdev.infrastructure.entity.enumeration;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum BlogType {
    BLOG("Normal HTML as content"),
    MARKDOWN("Markdown blog (minimal)"),
    VIDEO("Video-log"),
    TROLL("Troll-log");

    private final String description;
}
