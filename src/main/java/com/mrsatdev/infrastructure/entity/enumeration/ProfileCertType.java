package com.mrsatdev.infrastructure.entity.enumeration;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ProfileCertType {
    GLOBAL("Recognized International Certification"),
    SKILL("Skill Certificate"),
    COURSE("Course Certificate"),
    EVENT("Event Certificate");

    private final String description;
}
