package com.mrsatdev.infrastructure.entity.enumeration;

public enum RoleType {
    GUEST,
    USER,
    MODERATOR,
    ADMIN
}
