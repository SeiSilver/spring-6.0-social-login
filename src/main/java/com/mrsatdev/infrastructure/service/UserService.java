package com.mrsatdev.infrastructure.service;

import com.mrsatdev.application.dto.DataListResponse;
import com.mrsatdev.application.dto.request.user.UserBasicUpdateRequest;
import com.mrsatdev.infrastructure.entity.model.AuthUser;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;

public interface UserService {

    ResponseEntity<DataListResponse> getAllManagedPaging(String keyword, Pageable pageable);

    AuthUser getManagedById(Long id);

    void update(Long id, UserBasicUpdateRequest userUpdateDto);
}
