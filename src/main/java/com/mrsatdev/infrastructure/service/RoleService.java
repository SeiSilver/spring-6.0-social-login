package com.mrsatdev.infrastructure.service;

import com.mrsatdev.infrastructure.entity.model.AuthRole;

public interface RoleService {

    AuthRole getRoleUser();
}
