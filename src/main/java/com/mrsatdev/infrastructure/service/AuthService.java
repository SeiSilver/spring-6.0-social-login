package com.mrsatdev.infrastructure.service;

import com.mrsatdev.application.dto.request.user.AuthLoginRequest;
import com.mrsatdev.application.dto.request.user.UserRegisterRequest;
import com.mrsatdev.application.dto.response.user.UserLoginResponse;

public interface AuthService {

    String login(AuthLoginRequest authLoginRequest);

    UserLoginResponse getCurrentUser();

    void register(UserRegisterRequest userRegisterDto);
}
