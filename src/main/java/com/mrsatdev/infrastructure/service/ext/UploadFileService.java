package com.mrsatdev.infrastructure.service.ext;

import com.cloudinary.Cloudinary;
import com.mrsatdev.application.exception.common.InputException;
import com.mrsatdev.application.exception.common.OtherCommonException;
import com.mrsatdev.infrastructure.entity.enumeration.CloudinaryFolderType;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import static com.mrsatdev.domain.constant.error.CommonErrorConstant.ERROR_PROCESSING_IMG;
import static com.mrsatdev.domain.constant.error.InputErrorConstant.ERROR_IMG_TYPE;
import static org.springframework.http.MediaType.*;

@Service
@RequiredArgsConstructor
public class UploadFileService {

    private final Cloudinary cloudinary;

    @Transactional
    public String uploadImage(MultipartFile file, CloudinaryFolderType type, String folder) {
        if (file == null || file.isEmpty()) {
            return "";
        }

        if (!Arrays.asList(IMAGE_JPEG_VALUE, IMAGE_PNG_VALUE, IMAGE_GIF_VALUE).contains(file.getContentType())) {
            throw new InputException(ERROR_IMG_TYPE);
        }
        Map<String, java.io.Serializable> params = new HashMap<>();
        params.put("folder", String.format("MrSat/%s/%s", type.getValue(), folder));
        params.put("overwrite", false);

        String url;
        try {
            Map<?, ?> res = cloudinary.uploader().upload(file.getBytes(), params);
            url = cloudinary.url().secure(true).signed(true).generate((String) res.get("public_id"));
        } catch (Exception e) {
            throw new OtherCommonException(String.format(ERROR_PROCESSING_IMG, e.getMessage()));
        }
        return url;
    }
}
