package com.mrsatdev.infrastructure.service.ext;

import com.mrsatdev.application.exception.common.NotFoundException;
import com.mrsatdev.infrastructure.adapter.security.model.UserPrincipal;
import com.mrsatdev.infrastructure.entity.model.BlogData;
import com.mrsatdev.infrastructure.entity.model.ContactForm;
import com.mrsatdev.infrastructure.entity.model.ProfileCert;
import com.mrsatdev.infrastructure.repository.BlogRepository;
import com.mrsatdev.infrastructure.repository.ContactFormRepository;
import com.mrsatdev.infrastructure.repository.ProfileCertRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class PreAuthenticationVerifyService {

    private final BlogRepository blogRepository;
    private final ProfileCertRepository certRepository;
    private final ContactFormRepository contactFormRepository;

    public boolean verifyOwner(Long id) {
        return getThisPrincipalId().equals(id);
    }

    public boolean verifyCertOwner(Long id) throws NotFoundException {
        Optional<ProfileCert> profileCert = certRepository.findById(id);
        if (profileCert.isEmpty()) {
            throw new NotFoundException(ProfileCert.class.getName());
        }
        return getThisPrincipalId().equals(profileCert.get().getOwner().getId());
    }

    public boolean verifyContactFormOwner(Long id) throws NotFoundException {
        Optional<ContactForm> contactForm = contactFormRepository.findById(id);
        if (contactForm.isEmpty()) {
            throw new NotFoundException(ContactForm.class.getName());
        }
        return getThisPrincipalId().equals(contactForm.get().getRequester().getId());
    }

    public boolean verifyBlogOwner(Long id) throws NotFoundException {
        Optional<BlogData> blog = blogRepository.findById(id);
        if (blog.isEmpty()) {
            throw new NotFoundException(BlogData.class.getName());
        }
        return getThisPrincipalId().equals(blog.get().getWriter().getId());
    }

    private Long getThisPrincipalId() {
        UserPrincipal userPrincipal;
        try {
            userPrincipal = (UserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            return userPrincipal.getUser().getId();
        } catch (Exception e) {
            throw new AccessDeniedException("");
        }
    }
}
