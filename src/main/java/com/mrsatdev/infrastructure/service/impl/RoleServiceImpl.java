package com.mrsatdev.infrastructure.service.impl;

import com.mrsatdev.infrastructure.entity.model.AuthRole;
import com.mrsatdev.infrastructure.entity.enumeration.RoleType;
import com.mrsatdev.infrastructure.repository.RoleRepository;
import com.mrsatdev.infrastructure.service.RoleService;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class RoleServiceImpl implements RoleService {

    private final RoleRepository roleRepository;

    @Override
    public AuthRole getRoleUser() {
        return roleRepository.findByName(RoleType.USER.toString());
    }
}
