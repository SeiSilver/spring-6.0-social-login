package com.mrsatdev.infrastructure.service.impl;

import com.mrsatdev.application.dto.request.user.AuthLoginRequest;
import com.mrsatdev.application.dto.request.user.UserRegisterRequest;
import com.mrsatdev.application.dto.response.user.UserLoginResponse;
import com.mrsatdev.application.dto.response.user.UserLoginRoleResponse;
import com.mrsatdev.application.exception.auth.AuthBadRequestException;
import com.mrsatdev.domain.mapper.UserMapper;
import com.mrsatdev.infrastructure.adapter.security.model.UserPrincipal;
import com.mrsatdev.infrastructure.adapter.security.service.JWTService;
import com.mrsatdev.infrastructure.adapter.security.service.UserPrincipalDetailService;
import com.mrsatdev.infrastructure.entity.enumeration.AuthProviderType;
import com.mrsatdev.infrastructure.entity.enumeration.CloudinaryFolderType;
import com.mrsatdev.infrastructure.entity.model.AuthPermission;
import com.mrsatdev.infrastructure.entity.model.AuthUser;
import com.mrsatdev.infrastructure.repository.UserRepository;
import com.mrsatdev.infrastructure.service.AuthService;
import com.mrsatdev.infrastructure.service.RoleService;
import com.mrsatdev.infrastructure.service.ext.UploadFileService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Objects;

@Service
@RequiredArgsConstructor
@Slf4j
public class AuthServiceImpl implements AuthService {

    private final ModelMapper modelMapper;
    private final AuthenticationManager authenticationManager;
    private final RoleService roleService;
    private final JWTService jwtService;
    private final UserPrincipalDetailService userPrincipalDetailService;
    private final UploadFileService uploadFileService;
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    @Override
    public String login(AuthLoginRequest authLoginRequest) {
        this.authenticate(authLoginRequest);
        UserPrincipal userPrincipal = (UserPrincipal) userPrincipalDetailService.loadUserByUsername(authLoginRequest.getIdentifier());
        return jwtService.generateJwtToken(userPrincipal);
    }

    @Override
    @Transactional
    public UserLoginResponse getCurrentUser() {
        UserPrincipal userPrincipal;
        try {
            userPrincipal = (UserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        } catch (Exception e) {
            throw new AccessDeniedException("");
        }

        assert userPrincipal != null;
        AuthUser user = userPrincipal.getUser();

        UserLoginResponse userLoginResponse = UserMapper.toUserLoginResponse(modelMapper, user);
        userLoginResponse.setRole(new UserLoginRoleResponse(
                user.getAuthRole().getName(),
                userPrincipal.getAuthorities(),
                user.getAuthRole().getAuthPermissions().stream().map((AuthPermission::getDescription)).toList()
        ));
        return userLoginResponse;
    }

    @Override
    @Transactional
    public void register(UserRegisterRequest userRegisterRequest) {
        this.validateNewUserByIdentifier(userRegisterRequest.getEmail(), userRegisterRequest.getUsername());

        String url = uploadFileService.uploadImage(userRegisterRequest.getAvatar(), CloudinaryFolderType.USER_AVATAR, "");
        AuthUser user = AuthUser.builder()
                .avatarUrl(url)
                .username(userRegisterRequest.getUsername())
                .email(userRegisterRequest.getEmail())
                .fullName(userRegisterRequest.getFullName())
                .password(this.encodePassword(userRegisterRequest.getPassword()))
                .birthday(userRegisterRequest.getBirthday())
                .phoneNumber(userRegisterRequest.getPhone())
                .authProvider(AuthProviderType.LOCAL)
                .authRole(roleService.getRoleUser())
                .build();
        userRepository.save(user);
        //emailService.sendWelcomeMail(user.getFullName(), userRegisterDto.getEmail());
    }

    private void validateNewUserByIdentifier(String email, String username) {
        AuthUser userByNewEmail = userRepository.findByEmail(email).orElse(null);
        AuthUser userByNewUsername = userRepository.findByUsername(username).orElse(null);
        if (Objects.nonNull(userByNewEmail)) {
            throw new AuthBadRequestException(userByNewEmail.getEmail());
        }
        if (Objects.nonNull(userByNewUsername)) {
            throw new AuthBadRequestException(userByNewUsername.getUsername());
        }
    }

    private String encodePassword(String password) {
        return passwordEncoder.encode(password);
    }

    private void authenticate(AuthLoginRequest userLoginDto) {
        authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(userLoginDto.getIdentifier(), userLoginDto.getPassword()));
    }
}
