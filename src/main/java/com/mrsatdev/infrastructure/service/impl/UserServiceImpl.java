package com.mrsatdev.infrastructure.service.impl;

import com.mrsatdev.application.dto.DataListResponse;
import com.mrsatdev.application.dto.common.MetaDataResponse;
import com.mrsatdev.application.dto.request.user.UserBasicUpdateRequest;
import com.mrsatdev.application.exception.user.UserNotFoundException;
import com.mrsatdev.domain.helper.HttpHelper;
import com.mrsatdev.domain.mapper.UserMapper;
import com.mrsatdev.infrastructure.entity.enumeration.ActivationStatusType;
import com.mrsatdev.infrastructure.entity.model.AuthUser;
import com.mrsatdev.infrastructure.repository.UserRepository;
import com.mrsatdev.infrastructure.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.mrsatdev.domain.constant.DataResponseConstant.GET_OK;

@Service
@RequiredArgsConstructor
@Slf4j
public class UserServiceImpl implements UserService {

    private final ModelMapper modelMapper;
    private final UserRepository userRepository;

    @Override
    public ResponseEntity<DataListResponse> getAllManagedPaging(String keyword, Pageable pageable) {
        keyword = StringUtils.isNotBlank(keyword) ? keyword.trim() : StringUtils.EMPTY;

        Page<AuthUser> pageAuthUser = userRepository.findAllPaging(keyword, pageable);

        return HttpHelper.createDataListResponse(
                HttpStatus.OK,
                pageAuthUser.stream()
                        .map(i -> UserMapper.toUserManagedItemResponse(modelMapper, i))
                        .collect(Collectors.toList()),
                MetaDataResponse.builder()
                        .pageNumber(pageAuthUser.getNumber())
                        .pageSize(pageAuthUser.getSize())
                        .totalItems(pageAuthUser.getTotalElements()).build(),
                GET_OK);
    }

    @Override
    public AuthUser getManagedById(Long id) {
        AuthUser user = userRepository.findById(id).orElseThrow(UserNotFoundException::new);
        user.setPassword(null);
        return user;
    }

    @Override
    @Transactional
    public void update(Long id, UserBasicUpdateRequest userBasicUpdateRequest) {
        AuthUser user = this.findById(id);
        if (Objects.nonNull(userBasicUpdateRequest.getFullName())) {
            user.setFullName(userBasicUpdateRequest.getFullName());
        }
        userRepository.save(user);
    }

    private AuthUser findById(long id) {
        return Optional.ofNullable(userRepository.findByIdAndActivationStatus(id, ActivationStatusType.ACTIVE))
                .orElseThrow(UserNotFoundException::new);
    }
}
