package com.mrsatdev.infrastructure.adapter.cloudinary;

import com.cloudinary.Cloudinary;
import com.cloudinary.utils.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CloudinaryAdapter {

    @Value("${app.domain.cloudinary.cloud-name}")
    private String cloudName;
    @Value("${app.domain.cloudinary.api-key}")
    private String apiKey;
    @Value("${app.domain.cloudinary.api-secret}")
    private String apiSecret;

    @Bean
    public Cloudinary cloudinaryInitialize() {
        return new Cloudinary(ObjectUtils.asMap(
                "cloud_name", cloudName,
                "api_key", apiKey,
                "api_secret", apiSecret,
                "secure", true));
    }

}