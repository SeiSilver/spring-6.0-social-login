package com.mrsatdev.infrastructure.adapter.audit;

import org.springframework.data.domain.AuditorAware;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.Optional;

public class AuditorAwareImpl implements AuditorAware<String> {

    @Override
    public Optional<String> getCurrentAuditor() {
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        if (username == null || username.equalsIgnoreCase("anonymousUser")) return Optional.of("System Manipulated");
        return Optional.of(username);
    }
}