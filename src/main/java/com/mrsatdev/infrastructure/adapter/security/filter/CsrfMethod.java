package com.mrsatdev.infrastructure.adapter.security.filter;

import com.mrsatdev.infrastructure.entity.model.CSRFData;
import com.mrsatdev.infrastructure.repository.CsrfRepository;
import jakarta.servlet.http.HttpServletRequest;
import lombok.NoArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Base64;
import java.util.Objects;
import java.util.Optional;

@NoArgsConstructor
public class CsrfMethod {

    private static final Logger logger = LoggerFactory.getLogger(CsrfMethod.class);

    protected boolean passFilterCsrf(HttpServletRequest request, CsrfRepository csrfRepository, String secret, int method) {

        boolean isOption = Objects.equals(request.getMethod(), "OPTIONS");
        boolean isGet = Objects.equals(request.getMethod(), "GET");
        if (isOption || (isGet && Objects.equals(request.getRequestURI(), "/login"))
                || isGet && Objects.equals(request.getRequestURI(), "/healthcheck")) {
            return true;
        }

        try {
            String token = request.getHeader("X-MRSAT-TOKEN");
            String session = request.getHeader("X-MRSAT-SESSION");
            String key = request.getHeader("J-SESSION");

            Optional<CSRFData> exitsSession = csrfRepository.findBySessionAndKey(Long.parseLong(session), key);
            if (exitsSession.isPresent()) {
                return false;
            }

            String thisCredential = key.concat(secret.concat(session));
            String thisParsedCredential = new String(Base64.getEncoder().encode(thisCredential.getBytes()));
            int thisLength = thisParsedCredential.length();

            String thisCheckWithMethod = thisParsedCredential.substring(0, method)
                    .concat(thisParsedCredential.substring(thisLength - method, thisLength));

            if (thisCheckWithMethod.equals(token)) {
                csrfRepository.save(CSRFData.builder()
                        .key(key)
                        .session(Long.parseLong(session))
                        .build());
                return true;
            }
        } catch (Exception e) {
            logger.error("CSRF ERR: " + e.getMessage());
        }
        return false;
    }
}
