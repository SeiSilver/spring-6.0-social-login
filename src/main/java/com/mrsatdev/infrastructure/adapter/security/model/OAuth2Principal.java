package com.mrsatdev.infrastructure.adapter.security.model;

import java.util.Map;

public abstract class OAuth2Principal {
    protected Map<String, Object> attributes;

    public OAuth2Principal(Map<String, Object> attributes) {
        this.attributes = attributes;
    }

    public abstract String getId();

    public abstract String getName();

    public abstract String getEmail();

    public abstract String getImageUrl();
}
