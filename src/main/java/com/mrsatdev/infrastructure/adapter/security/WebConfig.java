package com.mrsatdev.infrastructure.adapter.security;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.List;

@Configuration
public class WebConfig {

    @Value("#{'${app.origin.allow}'.split(',\\s*')}")
    private List<String> allowOrigins;

    @Bean
    public WebMvcConfigurer cors() {
        return new WebMvcConfigurer() {
            @Override
            public void addCorsMappings(CorsRegistry reg) {
                reg.addMapping("/**").allowCredentials(true)
                        .allowedOriginPatterns(allowOrigins.toArray(new String[0]))
                        .allowedHeaders("Origin", "Access-Control-Allow-Origin", "Content-Type",
                                "Accept", "Jwt-Token", "Authorization", "X-MrSat-TOKEN", "X-MrSat-SESSION", "J-SESSION", "X-Requested-With",
                                "Access-Control-Request-Method", "Access-Control-Request-Headers")
                        .exposedHeaders("Origin", "Content-Type", "Accept", "Jwt-Token", "Authorization", "Access-Control-Allow-Origin", "Access-Control-Allow-Credentials")
                        .allowedMethods("GET", "POST", "PUT", "DELETE", "OPTIONS")
                        .maxAge(3600L);
            }
        };
    }
}
