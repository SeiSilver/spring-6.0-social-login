package com.mrsatdev.infrastructure.adapter.security.model;

import java.util.Map;

public class GithubUserInfo extends OAuth2Principal {

    public GithubUserInfo(Map<String, Object> attributes) {
        super(attributes);
    }

    public String getId() {
        return (String) attributes.get("id");
    }

    @Override
    public String getName() {
        return (String) attributes.get("name");
    }

    public String getEmail() {
        return (String) attributes.get("email");
    }

    public String getImageUrl() {
        return (String) attributes.get("avatar_url");
    }

}
