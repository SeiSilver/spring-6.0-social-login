package com.mrsatdev.infrastructure.adapter.security.service;

import com.mrsatdev.application.exception.user.UserNotFoundException;
import com.mrsatdev.infrastructure.adapter.security.model.UserPrincipal;
import com.mrsatdev.infrastructure.entity.enumeration.ActivationStatusType;
import com.mrsatdev.infrastructure.entity.model.AuthUser;
import com.mrsatdev.infrastructure.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class UserPrincipalDetailService implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String identifier) throws UsernameNotFoundException {
        AuthUser user = this.userRepository.findByIdentifierAndActivationStatus(identifier, ActivationStatusType.ACTIVE);

        if (user != null) {
            user.setLstLoggedDate(Timestamp.valueOf(LocalDateTime.now()));
            userRepository.save(user);
        } else {
            throw new BadCredentialsException("");
        }

        return UserPrincipal.create(user);
    }

    @Transactional
    public UserPrincipal getByEmail(String email) {
        AuthUser user = findByEmail(email);
        return UserPrincipal.create(user);
    }

    private AuthUser findByEmail(String email) {
        return Optional.ofNullable(userRepository.findByEmailAndActivationStatus(email, ActivationStatusType.ACTIVE))
                .orElseThrow(UserNotFoundException::new);
    }
}
