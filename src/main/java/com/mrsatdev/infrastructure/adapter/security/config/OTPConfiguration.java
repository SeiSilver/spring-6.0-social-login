package com.mrsatdev.infrastructure.adapter.security.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Data
@Configuration
@ConfigurationProperties(prefix = "twilio")
public class OTPConfiguration {

    @Value("${app.domain.twilio.account-sid}")
    private String accountSid;

    @Value("${app.domain.twilio.auth-token}")
    private String authToken;

    @Value("${app.domain.twilio.trial-number}")
    private String trialNumber;
}
