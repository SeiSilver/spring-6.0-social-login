package com.mrsatdev.infrastructure.adapter.security.filter;

import com.mrsatdev.application.dto.GenericResponse;
import com.mrsatdev.infrastructure.adapter.security.model.UserPrincipal;
import com.mrsatdev.infrastructure.adapter.security.service.JWTService;
import com.mrsatdev.infrastructure.adapter.security.service.UserPrincipalDetailService;
import com.mrsatdev.infrastructure.repository.CsrfRepository;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import static com.mrsatdev.domain.constant.error.SecurityErrorConstant.CSRF_ACCESS_DENIED_MESSAGE;
import static com.mrsatdev.infrastructure.adapter.security.config.JWTConfiguration.HEADER_AUTHORIZATION;
import static com.mrsatdev.infrastructure.adapter.security.config.JWTConfiguration.TOKEN_PREFIX;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;

@Component
@RequiredArgsConstructor
public class JwtAuthorizationFilter extends OncePerRequestFilter {

    private static final Logger logger = LoggerFactory.getLogger(JwtAuthorizationFilter.class);

    @Value("${app.auth.csrf.enable}")
    private boolean csrfEnabled;

    @Value("${app.auth.csrf.secret}")
    private String secret;

    @Value("${app.auth.csrf.method}")
    private int method;

    private final JWTService jwtService;
    private final UserPrincipalDetailService userPrincipalDetailService;
    private final CsrfRepository csrfRepository;

    @Override
    protected void doFilterInternal(@NonNull HttpServletRequest request, @NonNull HttpServletResponse response, @NonNull FilterChain filterChain)
            throws ServletException, IOException {
        if (csrfEnabled) {
            boolean passFilterCsrf = new CsrfMethod().passFilterCsrf(request, csrfRepository, secret, method);
            if (!passFilterCsrf) {
                logger.info("Filter chain ERR: CSRF");
                response.sendError(HttpServletResponse.SC_UNAUTHORIZED, CSRF_ACCESS_DENIED_MESSAGE);
                return;
            }
        }

        logger.info("Filter chain: Checking header for request [{}], Authorization [{}]",
                request.getRequestURI(),
                Objects.nonNull(request.getHeader("Authorization")));

        if (hasAuthorizationBearer(request)) {
            try {
                logger.info("Filter chain: Bearer Authorization...");
                String authorizationHeader = request.getHeader(AUTHORIZATION);
                String token = authorizationHeader.substring(TOKEN_PREFIX.length());
                String email = jwtService.getSubject(token);
                UserPrincipal userPrincipal = userPrincipalDetailService.getByEmail(email);

                if (jwtService.isTokenValid(email, token) && Objects.isNull(SecurityContextHolder.getContext().getAuthentication())) {
                    List<GrantedAuthority> authorities = jwtService.getAuthorities(token);
                    Authentication authentication = jwtService.getAuthentication(userPrincipal, authorities, request);
                    SecurityContextHolder.getContext().setAuthentication(authentication);
                } else {
                    SecurityContextHolder.clearContext();
                }
            } catch (Exception e) {
                logger.error("Filter chain ERR: " + e.getMessage());
            }
        }

        filterChain.doFilter(request, response);
    }

    protected static GenericResponse setHttpAuthorizationResponse(HttpStatus httpStatus, String message) {
        Map<String, String> errors = new HashMap<>();
        errors.put(httpStatus.toString(), message);
        return GenericResponse.builder()
                .timeStamp(LocalDateTime.now())
                .httpStatus(httpStatus)
                .errors(errors)
                .build();
    }

    private boolean hasAuthorizationBearer(HttpServletRequest request) {
        String header = request.getHeader(HEADER_AUTHORIZATION);
        return !ObjectUtils.isEmpty(header) && header.startsWith(TOKEN_PREFIX);
    }
}
