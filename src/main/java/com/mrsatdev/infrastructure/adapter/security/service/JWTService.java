package com.mrsatdev.infrastructure.adapter.security.service;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.mrsatdev.infrastructure.adapter.security.model.UserPrincipal;
import jakarta.servlet.http.HttpServletRequest;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import static com.mrsatdev.infrastructure.adapter.security.config.JWTConfiguration.*;

@Service
@NoArgsConstructor
public class JWTService {

    @Value("${app.auth.jwt.secret}")
    private String secretKey;

    @Value("${app.auth.jwt.expiration-msec}")
    private long jwtExpiration;

    public String generateJwtToken(UserPrincipal userPrincipal) {
        Algorithm algorithm = Algorithm.HMAC512(secretKey.getBytes());
        String[] claims = this.getClaimsFromUser(userPrincipal);
        return JWT.create()
                .withIssuer(ISSUER)
                .withAudience(ADMINISTRATION).withIssuedAt(new Date())
                .withSubject(userPrincipal.getUser().getEmail())
                .withArrayClaim(AUTHORITIES, claims)
                .withExpiresAt(new Date(System.currentTimeMillis() + jwtExpiration))
                .sign(algorithm);
    }

    public List<GrantedAuthority> getAuthorities(String token) {
        String[] claims = getClaimsFromToken(token);
        return Arrays.stream(claims).map(SimpleGrantedAuthority::new).collect(Collectors.toList());
    }

    public Authentication getAuthentication(UserPrincipal userPrincipal, List<GrantedAuthority> authorities, HttpServletRequest request) {
        UsernamePasswordAuthenticationToken usernamePasswordAuthToken = new UsernamePasswordAuthenticationToken(userPrincipal, null, authorities);
        usernamePasswordAuthToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
        return usernamePasswordAuthToken;
    }

    public String getSubject(String token) {
        JWTVerifier verifier = this.getJWTVerifier();
        return verifier.verify(token).getSubject();
    }

    public boolean isTokenValid(String username, String token) {
        JWTVerifier verifier = this.getJWTVerifier();
        return StringUtils.isNotEmpty(username) && isNotTokenExpired(verifier, token);
    }

    private String[] getClaimsFromUser(UserPrincipal userPrincipal) {
        List<String> authorities = new ArrayList<>();
        userPrincipal.getAuthorities().forEach(authority -> authorities.add(authority.getAuthority()));
        return authorities.toArray(new String[0]);
    }

    private String[] getClaimsFromToken(String token) {
        JWTVerifier verifier = this.getJWTVerifier();
        return verifier.verify(token).getClaim(AUTHORITIES).asArray(String.class);
    }

    private JWTVerifier getJWTVerifier() {
        JWTVerifier verifier;
        try {
            Algorithm algorithm = Algorithm.HMAC512(secretKey);
            verifier = JWT.require(algorithm).withIssuer(ISSUER).build();
        } catch (JWTVerificationException e) {
            throw new JWTVerificationException(JWT_VALIDATION_FAIL);
        }
        return verifier;
    }

    private boolean isNotTokenExpired(JWTVerifier verifier, String token) {
        Date tokenExpiredDate = verifier.verify(token).getExpiresAt();
        return tokenExpiredDate.after(new Date());
    }
}

