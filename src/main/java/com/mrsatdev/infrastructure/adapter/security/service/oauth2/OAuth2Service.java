package com.mrsatdev.infrastructure.adapter.security.service.oauth2;

import com.mrsatdev.application.exception.auth.OAuth2AuthenticationProcessingException;
import com.mrsatdev.infrastructure.adapter.security.model.*;
import com.mrsatdev.infrastructure.entity.enumeration.AuthProviderType;
import com.mrsatdev.infrastructure.entity.model.AuthUser;
import com.mrsatdev.infrastructure.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.oauth2.client.userinfo.DefaultOAuth2UserService;
import org.springframework.security.oauth2.client.userinfo.OAuth2UserRequest;
import org.springframework.security.oauth2.core.OAuth2AuthenticationException;
import org.springframework.security.oauth2.core.user.OAuth2User;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class OAuth2Service extends DefaultOAuth2UserService {

    private final UserRepository userRepository;
    public static final String GOOGLE_PROVIDER = "google";
    public static final String FACEBOOK_PROVIDER = "facebook";
    public static final String GITHUB_PROVIDER = "github";

    @Override
    public OAuth2User loadUser(OAuth2UserRequest userRequest) throws OAuth2AuthenticationException {
        String regId = userRequest.getClientRegistration().getClientName();
        return switch (regId) {
            case GOOGLE_PROVIDER ->
                    processOAuth2User(userRequest, new GoogleUserInfo(userRequest.getAdditionalParameters()));
            case FACEBOOK_PROVIDER ->
                    processOAuth2User(userRequest, new FacebookUserInfo(userRequest.getAdditionalParameters()));
            case GITHUB_PROVIDER ->
                    processOAuth2User(userRequest, new GithubUserInfo(userRequest.getAdditionalParameters()));
            default ->
                    throw new OAuth2AuthenticationProcessingException("Sorry! Login with " + regId + " is not supported yet.");
        };
    }

    private OAuth2User processOAuth2User(OAuth2UserRequest oAuth2UserRequest, OAuth2Principal oAuth2Principal) {
        if (StringUtils.isEmpty(oAuth2Principal.getEmail())) {
            throw new OAuth2AuthenticationProcessingException("Email not found from OAuth2 provider");
        }

        Optional<AuthUser> userOptional = userRepository.findByEmail(oAuth2Principal.getEmail());
        AuthUser authUserResponse;
        if (userOptional.isPresent()) {
            authUserResponse = userOptional.get();
            AuthProviderType currentProvider = authUserResponse.getAuthProvider();
            if (!currentProvider.equals(AuthProviderType.valueOf(oAuth2UserRequest.getClientRegistration().getRegistrationId()))) {
                throw new OAuth2AuthenticationProcessingException("Looks like you're signed up with " +
                        currentProvider + " account. Please use your " + currentProvider +
                        " account to login.");
            }
            authUserResponse = updateExistingUser(authUserResponse, oAuth2Principal);
        } else {
            authUserResponse = registerNewUser(oAuth2UserRequest, oAuth2Principal);
        }

        return UserPrincipal.create(authUserResponse);
    }

    private AuthUser registerNewUser(OAuth2UserRequest oAuth2UserRequest, OAuth2Principal oAuth2UserInfo) {
        AuthUser user = new AuthUser();

        user.setAuthProvider(AuthProviderType.valueOf(oAuth2UserRequest.getClientRegistration().getRegistrationId()));
        user.setFullName(oAuth2UserInfo.getName());
        user.setEmail(oAuth2UserInfo.getEmail());
        user.setAvatarUrl(oAuth2UserInfo.getImageUrl());
        return userRepository.save(user);
    }

    private AuthUser updateExistingUser(AuthUser existingUser, OAuth2Principal oAuth2UserInfo) {
//        existingUser.setFullName(oAuth2UserInfo.getName());
//        existingUser.setAvatarUrl(oAuth2UserInfo.getImageUrl());
        return userRepository.save(existingUser);
    }

}
