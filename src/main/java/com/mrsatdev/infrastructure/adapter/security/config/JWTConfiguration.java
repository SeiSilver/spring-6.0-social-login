package com.mrsatdev.infrastructure.adapter.security.config;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class JWTConfiguration {
    public static final String ISSUER = "MRSATDEV";
    public static final String ADMINISTRATION = "MrSat Administration";
    public static final String AUTHORITIES = "Access";
    public static final String HEADER_AUTHORIZATION = "Authorization";
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String JWT_TOKEN_HEADER = "Jwt-Token";
    public static final String JWT_VALIDATION_FAIL = "Access denied";
}
