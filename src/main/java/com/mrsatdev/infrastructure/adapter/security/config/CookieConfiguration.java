package com.mrsatdev.infrastructure.adapter.security.config;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class CookieConfiguration {
    public static final String OAUTH2_REQUEST_COOKIE_NAME = "oauth2_auth_request";
    public static final String REDIRECT_COOKIE_NAME = "redirect_uri";
    public static final int COOKIE_EXPIRE_SECOND = 180;
}
