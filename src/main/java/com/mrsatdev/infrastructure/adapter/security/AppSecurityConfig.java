package com.mrsatdev.infrastructure.adapter.security;

import com.mrsatdev.infrastructure.adapter.security.filter.JwtAuthorizationFilter;
import com.mrsatdev.infrastructure.adapter.security.service.UserPrincipalDetailService;
import com.mrsatdev.infrastructure.adapter.security.service.oauth2.HttpCookieOAuth2AuthorizationRequestRepository;
import com.mrsatdev.infrastructure.adapter.security.service.oauth2.OAuth2AuthenticationFailureHandler;
import com.mrsatdev.infrastructure.adapter.security.service.oauth2.OAuth2AuthenticationSuccessHandler;
import com.mrsatdev.infrastructure.adapter.security.service.oauth2.OAuth2Service;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.configuration.AuthenticationConfiguration;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.oauth2.client.registration.ClientRegistration;
import org.springframework.security.oauth2.client.registration.ClientRegistrationRepository;
import org.springframework.security.oauth2.client.registration.InMemoryClientRegistrationRepository;
import org.springframework.security.oauth2.core.AuthorizationGrantType;
import org.springframework.security.oauth2.core.ClientAuthenticationMethod;
import org.springframework.security.oauth2.core.oidc.IdTokenClaimNames;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import static com.mrsatdev.infrastructure.adapter.security.service.oauth2.OAuth2Service.*;

@Configuration
@EnableWebSecurity
@EnableMethodSecurity
@RequiredArgsConstructor
public class AppSecurityConfig {

    @Value("${spring.security.oauth2.client.registration.google.clientId}")
    private String ggClientId;

    @Value("${spring.security.oauth2.client.registration.google.clientSecret}")
    private String ggClientSecret;

    @Value("${spring.security.oauth2.client.registration.facebook.clientId}")
    private String fbClientId;

    @Value("${spring.security.oauth2.client.registration.facebook.clientSecret}")
    private String fbClientSecret;

    @Value("${spring.security.oauth2.client.registration.github.clientId}")
    private String githubClientId;

    @Value("${spring.security.oauth2.client.registration.github.clientSecret}")
    private String githubClientSecret;

    private final OAuth2Service oAuth2Service;
    private final JwtAuthorizationFilter jwtAuthenticationFilter;
    private final UserPrincipalDetailService userPrincipalDetailService;
    private final OAuth2AuthenticationSuccessHandler oAuth2AuthenticationSuccessHandler;
    private final OAuth2AuthenticationFailureHandler oAuth2AuthenticationFailureHandler;

    private static final String[] ENDPOINTS_WHITELIST = {
            "/swagger-ui/**",
            "/swagger-ui.html/**",
            "/v3/api-docs/**",
            "/healthcheck",
            "/csrf-token",
            "/oauth2/**",
            "/auth/**",
            "/public/**"
    };

    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public UserDetailsService userDetailsService() {
        return new UserPrincipalDetailService();
    }

    @Bean
    public HttpCookieOAuth2AuthorizationRequestRepository cookieAuthorizationRequestRepository() {
        return new HttpCookieOAuth2AuthorizationRequestRepository();
    }

    @Bean
    public SecurityFilterChain filterChain(HttpSecurity httpSecurity) throws Exception {
        return httpSecurity
                .csrf(AbstractHttpConfigurer::disable)
                .authorizeHttpRequests(auth -> auth
                        .requestMatchers(ENDPOINTS_WHITELIST).permitAll()
                        .anyRequest().authenticated()
                )
                .sessionManagement(sess -> sess.sessionCreationPolicy(SessionCreationPolicy.STATELESS))
                .authenticationProvider(authenticationProvider()).addFilterBefore(
                        jwtAuthenticationFilter, UsernamePasswordAuthenticationFilter.class)
                .oauth2Login((oauth2Login) -> oauth2Login
                        .authorizationEndpoint(conf -> conf.authorizationRequestRepository(cookieAuthorizationRequestRepository()).baseUri("/oauth2/authorize"))
                        .redirectionEndpoint(conf -> conf.baseUri("/oauth2/callback/*"))
                        .userInfoEndpoint((userInfo) -> userInfo
                                .userService(oAuth2Service)
                        )
                        .successHandler(oAuth2AuthenticationSuccessHandler)
                        .failureHandler(oAuth2AuthenticationFailureHandler)
                )
                .formLogin(AbstractHttpConfigurer::disable)
                .build();
    }

    @Bean
    public ClientRegistrationRepository clientRegistrationRepository() {
        return new InMemoryClientRegistrationRepository(
                this.googleClientRegistration(),
                this.fbClientRegistration(),
                this.githubClientRegistration()
        );
    }

    @Bean
    public AuthenticationProvider authenticationProvider() {
        DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();
        authProvider.setUserDetailsService(userPrincipalDetailService);
        authProvider.setPasswordEncoder(passwordEncoder());
        return authProvider;
    }

    @Bean
    public AuthenticationManager authenticationManager(AuthenticationConfiguration authenticationConfiguration) throws Exception {
        return authenticationConfiguration.getAuthenticationManager();
    }

    private ClientRegistration googleClientRegistration() {
        return ClientRegistration.withRegistrationId(GOOGLE_PROVIDER)
                .clientId(ggClientId)
                .clientSecret(ggClientSecret)
                .clientAuthenticationMethod(ClientAuthenticationMethod.CLIENT_SECRET_BASIC)
                .authorizationGrantType(AuthorizationGrantType.AUTHORIZATION_CODE)
                .redirectUri("{baseUrl}/oauth2/callback/{registrationId}")
                .scope("openid", "profile", "email")
                .authorizationUri("https://accounts.google.com/o/oauth2/v2/auth")
                .tokenUri("https://www.googleapis.com/oauth2/v4/token")
                .userInfoUri("https://www.googleapis.com/oauth2/v3/userinfo")
                .userNameAttributeName(IdTokenClaimNames.SUB)
                .issuerUri("https://accounts.google.com")
                .jwkSetUri("https://www.googleapis.com/oauth2/v3/certs")
                .clientName(GOOGLE_PROVIDER)
                .build();
    }

    private ClientRegistration fbClientRegistration() {
        return ClientRegistration.withRegistrationId(FACEBOOK_PROVIDER)
                .clientId(fbClientId)
                .clientSecret(fbClientSecret)
                .clientAuthenticationMethod(ClientAuthenticationMethod.CLIENT_SECRET_BASIC)
                .authorizationGrantType(AuthorizationGrantType.AUTHORIZATION_CODE)
                .redirectUri("{baseUrl}/oauth2/callback/{registrationId}")
                .scope("openid", "public_profile", "email")
                .authorizationUri("https://www.facebook.com/v3.0/dialog/oauth")
                .tokenUri("https://graph.facebook.com/v3.0/oauth/access_token")
                .userInfoUri("https://graph.facebook.com/v3.0/me?fields=id,first_name,middle_name,last_name,name,email,verified,is_verified,picture.width(250).height(250)")
                .userNameAttributeName(IdTokenClaimNames.SUB)
                .clientName(FACEBOOK_PROVIDER)
                .build();
    }

    private ClientRegistration githubClientRegistration() {
        return ClientRegistration.withRegistrationId(GITHUB_PROVIDER)
                .clientId(githubClientId)
                .clientSecret(githubClientSecret)
                .clientAuthenticationMethod(ClientAuthenticationMethod.CLIENT_SECRET_BASIC)
                .authorizationGrantType(AuthorizationGrantType.AUTHORIZATION_CODE)
                .redirectUri("{baseUrl}/oauth2/callback/{registrationId}")
                .scope("user:email", "read:user")
                .authorizationUri("https://github.com/login/oauth/authorize")
                .tokenUri("s")
                .userNameAttributeName(IdTokenClaimNames.SUB)
                .clientName(GITHUB_PROVIDER)
                .build();
    }
}
