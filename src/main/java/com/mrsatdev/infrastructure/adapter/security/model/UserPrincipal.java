package com.mrsatdev.infrastructure.adapter.security.model;

import com.mrsatdev.infrastructure.entity.enumeration.ActivationStatusType;
import com.mrsatdev.infrastructure.entity.model.AuthUser;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.oauth2.core.user.OAuth2User;

import java.util.Arrays;
import java.util.Collection;
import java.util.Map;
import java.util.stream.Collectors;

@Data
@NoArgsConstructor
public class UserPrincipal implements UserDetails, OAuth2User {

    private AuthUser user;
    private Map<String, Object> attributes;

    private UserPrincipal(AuthUser user) {
        this.user = user;
    }

    public static UserPrincipal create(AuthUser user) {
        return new UserPrincipal(user);
    }

    @Override
    public String getName() {
        return this.user.getFullName();
    }

    @Override
    public String getPassword() {
        return this.user.getPassword();
    }

    @Override
    public String getUsername() {
        return this.user.getEmail();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        if (this.user == null) return false;
        return this.user.getActivationStatus() != ActivationStatusType.LOCKED;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return this.user.getActivationStatus() == ActivationStatusType.ACTIVE;
    }

    @Override
    public Map<String, Object> getAttributes() {
        return this.attributes;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        if (user.getAuthRole() == null) {
            return Arrays.<GrantedAuthority>asList(
                    new SimpleGrantedAuthority("MANAGE_OWN"),
                    new SimpleGrantedAuthority("SUBMIT_CONTENT")
            );
        }
        return this.user.getAuthRole().getAuthPermissions()
                .stream()
                .map(permission -> new SimpleGrantedAuthority(permission.getCode()))
                .collect(Collectors.toList());
    }
}