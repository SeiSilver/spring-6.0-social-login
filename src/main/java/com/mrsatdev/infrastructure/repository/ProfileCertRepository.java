package com.mrsatdev.infrastructure.repository;

import com.mrsatdev.infrastructure.entity.model.ProfileCert;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Repository
@Transactional
public interface ProfileCertRepository extends JpaRepository<ProfileCert, Long> {

    @Query(value = "SELECT * FROM profile_cert pc " +
            "WHERE(pc.name LIKE %?1% OR pc.organization LIKE %?1%)", nativeQuery = true)
    Page<ProfileCert> findAllPaging(String keyword, Pageable pageable);

    @Query(value = "SELECT * FROM profile_cert pc " +
            "WHERE(pc.name LIKE %?1% OR pc.organization LIKE %?1%) " +
            "AND(pc.activate_status = 'ACTIVE' ) " +
            "AND(pc.owner = %?2%)", nativeQuery = true)
    Page<ProfileCert> findAllPagingById(String keyword, Long owner, Pageable pageable);

    @Query(value = "SELECT * FROM profile_cert pc " +
            "WHERE(pc.id = %?1%) " +
            "AND(pc.activate_status = 'ACTIVE' )", nativeQuery = true)
    Optional<ProfileCert> findById(Long id);
}
