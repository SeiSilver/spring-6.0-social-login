package com.mrsatdev.infrastructure.repository;

import com.mrsatdev.infrastructure.entity.enumeration.ActivationStatusType;
import com.mrsatdev.infrastructure.entity.model.AuthUser;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Repository
@Transactional
public interface UserRepository extends JpaRepository<AuthUser, Long> {

    @Query(value = "SELECT * FROM auth_user u " +
            "WHERE(u.username LIKE %?1% OR u.full_name LIKE %?1%)", nativeQuery = true)
    Page<AuthUser> findAllPaging(String keyword, Pageable pageable);

    @Query(value = "SELECT * FROM auth_user u " +
            "WHERE(u.username LIKE %?1% OR u.full_name LIKE %?1%) " +
            "AND(u.activate_status = 'ACTIVE' )", nativeQuery = true)
    Page<AuthUser> findAllPagingAndActive(String keyword, Pageable pageable);

    Optional<AuthUser> findById(Long id);

    AuthUser findByIdAndActivationStatus(Long id, ActivationStatusType activationStatusType);

    @Query(value = "SELECT * FROM auth_user WHERE (username = ?1 OR email = ?1) AND activation_status = :#{#activationStatusType?.name()}", nativeQuery = true)
    AuthUser findByIdentifierAndActivationStatus(String identifier, ActivationStatusType activationStatusType);

    Optional<AuthUser> findByEmail(String email);

    AuthUser findByEmailAndActivationStatus(String email, ActivationStatusType activationStatus);

    Optional<AuthUser> findByUsername(String username);
}
