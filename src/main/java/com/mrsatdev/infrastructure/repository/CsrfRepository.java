package com.mrsatdev.infrastructure.repository;

import com.mrsatdev.infrastructure.entity.model.CSRFData;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Repository
@Transactional
public interface CsrfRepository extends JpaRepository<CSRFData, Long> {

    Optional<CSRFData> findBySessionAndKey(Long session, String key);
}
