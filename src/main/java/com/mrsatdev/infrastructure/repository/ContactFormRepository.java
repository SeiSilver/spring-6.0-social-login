package com.mrsatdev.infrastructure.repository;

import com.mrsatdev.infrastructure.entity.model.ContactForm;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Repository
@Transactional
public interface ContactFormRepository extends JpaRepository<ContactForm, Long> {

    @Query(value = "SELECT * FROM contact c " +
            "WHERE(c.title LIKE %?1%)", nativeQuery = true)
    Page<ContactForm> findAllPaging(String keyword, Pageable pageable);

    @Query(value = "SELECT * FROM contact c " +
            "WHERE(c.title LIKE %?1%) " +
            "AND(c.activate_status = 'ACTIVE' ) " +
            "AND(c.user = %?2%)", nativeQuery = true)
    Page<ContactForm> findAllPagingById(String keyword, Long user, Pageable pageable);

    @Query(value = "SELECT * FROM contact c " +
            "WHERE(c.id = %?1%) " +
            "AND(c.activate_status = 'ACTIVE' )", nativeQuery = true)
    Optional<ContactForm> findById(Long id);

    @Query(value = "SELECT * FROM contact c " +
            "WHERE(c.parent_id = %?1%) " +
            "AND(c.activate_status = 'ACTIVE' )", nativeQuery = true)
    Optional<ContactForm> findAllChildById(Long parentId);
}
