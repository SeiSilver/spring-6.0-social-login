package com.mrsatdev.infrastructure.repository;

import com.mrsatdev.infrastructure.entity.model.BlogData;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Repository
@Transactional
public interface BlogRepository extends JpaRepository<BlogData, Long> {

    @Query(value = "SELECT * FROM blog_data b " +
            "WHERE(b.title LIKE %?1% OR b.description LIKE %?1%) " +
            "AND(b.type != 'TROLL') " +
            "AND(b.status = 'PUBLISHED') " +
            "AND(b.activate_status = 'ACTIVE' ) " +
            "ORDER BY (b.lst_publish_date DESC)", nativeQuery = true)
    Page<BlogData> findAllTopPagePaginate(String keyword, Pageable pageable);


    @Query(value = "SELECT * FROM blog_data b " +
            "WHERE(b.title LIKE %?1% OR b.description LIKE %?1%) " +
            "AND(b.type != 'TROLL') " +
            "AND(b.status = 'PUBLISHED') " +
            "AND(b.activate_status = 'ACTIVE' ) " +
            "ORDER BY (b.lst_publish_date DESC)", nativeQuery = true)
    Page<BlogData> findAllManagedPaginate(String keyword, Pageable pageable);

    @Query(value = "SELECT * FROM blog_data b " +
            "WHERE(b.id = %?1%) " +
            "AND(b.activate_status = 'ACTIVE' )", nativeQuery = true)
    Optional<BlogData> findById(Long id);
}
