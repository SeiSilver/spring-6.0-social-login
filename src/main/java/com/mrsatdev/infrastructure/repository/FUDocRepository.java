package com.mrsatdev.infrastructure.repository;

import com.mrsatdev.infrastructure.entity.model.FUCourse;
import com.mrsatdev.infrastructure.entity.model.FUDocument;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Repository
@Transactional
public interface FUDocRepository extends JpaRepository<FUCourse, Long> {

    @Query(value = "SELECT * FROM fu_course course " +
            "WHERE(course.title LIKE %?1% OR course.description LIKE %?1%)", nativeQuery = true)
    Page<FUCourse> findAllCoursePaging(String keyword, Pageable pageable);

    @Query(value = "SELECT * FROM fu_document doc " +
            "WHERE(doc.title LIKE %?1% OR doc.code LIKE %?1%)", nativeQuery = true)
    Page<FUDocument> findAllDocumentPaging(String keyword, Pageable pageable);

    @Query(value = "SELECT * FROM fu_course_document fcd " +
            "RIGHT JOIN fu_document doc ON fcd.document_id = doc.id " +
            "WHERE(fcd.course_id = %?1%)", nativeQuery = true)
    Page<FUDocument> findAllByCoursePaging(Long courseId, Pageable pageable);

    @Query(value = "SELECT * FROM fu_document doc " +
            "WHERE(doc.id = %?1%) " +
            "AND(doc.activate_status = 'ACTIVE' )", nativeQuery = true)
    Optional<FUDocument> findDocById(Long id);
}
