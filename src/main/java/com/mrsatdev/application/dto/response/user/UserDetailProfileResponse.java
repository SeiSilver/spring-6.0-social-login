package com.mrsatdev.application.dto.response.user;

import com.mrsatdev.application.dto.common.BaseDataResponse;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.sql.Timestamp;
import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserDetailProfileResponse extends BaseDataResponse {

    private String username;

    private String email;

    private String fullName;

    private String mainTitle;

    private String avatarUrl;

    private String bio;

    private LocalDate birthday;

    private Timestamp lstUpdatedDate;

    private Timestamp lstLoggedDate;

    private Timestamp joinOn;

    private String roleCode;
}
