package com.mrsatdev.application.dto.response.blog;

import com.mrsatdev.application.dto.response.user.UserPublicProfileResponse;
import com.mrsatdev.infrastructure.entity.enumeration.BlogStatus;
import com.mrsatdev.infrastructure.entity.enumeration.BlogType;
import com.mrsatdev.infrastructure.entity.model.BlogCategory;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.sql.Timestamp;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class BlogManagedResponse {

    private Long id;

    private String friendlyName;

    private String title;

    private String description;

    private String pictureUrl;

    private BlogType type;

    private UserPublicProfileResponse publisher;

    private Timestamp lstPublishDate;

    private BlogStatus status;

    private List<BlogCategory> categories;

    private String html;

    private String videoUrl;

    private Integer view;

    private Timestamp createdAt;

    private Timestamp updatedAt;
}
