package com.mrsatdev.application.dto.response.fu_doc;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CourseItemResponse {

    private Long id;

    private String title;

    private String pictureURL;

    private Integer cost;

    private Integer view;
}
