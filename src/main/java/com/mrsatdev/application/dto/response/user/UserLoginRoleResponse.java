package com.mrsatdev.application.dto.response.user;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Collection;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserLoginRoleResponse {

    private String roleCode;

    private Collection<?> authority;

    private Collection<?> description;
}
