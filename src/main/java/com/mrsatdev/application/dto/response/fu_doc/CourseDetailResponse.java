package com.mrsatdev.application.dto.response.fu_doc;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CourseDetailResponse {

    private String title;

    private String description;

    private String pictureURL;

    private Integer cost;

    private Integer view;

    private ArrayList<DocumentDetailResponse> documents;
}
