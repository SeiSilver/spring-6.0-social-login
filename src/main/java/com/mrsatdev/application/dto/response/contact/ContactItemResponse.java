package com.mrsatdev.application.dto.response.contact;

import com.mrsatdev.application.dto.response.user.UserPublicProfileResponse;
import com.mrsatdev.infrastructure.entity.enumeration.ContactFormType;
import com.mrsatdev.infrastructure.entity.enumeration.ContactStatusType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ContactItemResponse {

    private Long id;

    private ContactStatusType status;

    private ContactFormType type;

    private UserPublicProfileResponse requester;

    private String title;

    private Timestamp createdAt;
}
