package com.mrsatdev.application.dto.response.fu_doc;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DocumentDetailResponse {

    private Long id;

    private String title;

    private String description;

    private String pictureURL;

    private String link;

    private String code;

    private List<String> changeDescription;

    private Integer view;
}
