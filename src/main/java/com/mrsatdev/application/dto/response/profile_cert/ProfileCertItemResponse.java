package com.mrsatdev.application.dto.response.profile_cert;

import com.mrsatdev.infrastructure.entity.enumeration.ProfileCertType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.web.multipart.MultipartFile;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ProfileCertItemResponse {

    private Long id;

    private ProfileCertType type;

    private String code;

    private String name;

    private MultipartFile picture;
}
