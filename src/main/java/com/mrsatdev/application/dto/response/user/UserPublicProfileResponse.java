package com.mrsatdev.application.dto.response.user;

import com.mrsatdev.application.dto.common.BaseDataResponse;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.sql.Timestamp;
import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserPublicProfileResponse extends BaseDataResponse {

    private String username;

    private String fullName;

    private String mainTitle;

    private String bio;

    private String avatarUrl;

    private String roleCode;

    private LocalDate birthday;

    private Timestamp lstLoggedDate;

    private Timestamp joinOn;
}
