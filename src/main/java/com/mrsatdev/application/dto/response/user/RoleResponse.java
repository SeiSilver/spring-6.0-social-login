package com.mrsatdev.application.dto.response.user;

import com.mrsatdev.application.dto.common.BaseDataResponse;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class RoleResponse extends BaseDataResponse {

    private String name;
    private List<String> permissions;
}
