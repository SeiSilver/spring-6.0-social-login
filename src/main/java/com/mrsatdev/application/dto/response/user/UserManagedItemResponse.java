package com.mrsatdev.application.dto.response.user;

import com.mrsatdev.application.dto.common.BaseDataResponse;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserManagedItemResponse extends BaseDataResponse {

    private String id;

    private String username;

    private String email;

    private String fullName;

    private String avatarUrl;

    private String roleCode;
}
