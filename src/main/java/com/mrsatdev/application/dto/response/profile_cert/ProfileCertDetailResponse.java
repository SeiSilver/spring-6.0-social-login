package com.mrsatdev.application.dto.response.profile_cert;

import com.mrsatdev.application.dto.response.user.UserPublicProfileResponse;
import com.mrsatdev.infrastructure.entity.enumeration.ProfileCertType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ProfileCertDetailResponse {

    private ProfileCertType type;

    private String code;

    private String organization;

    private String name;

    private String detail;

    private String pictureUrl;

    private String pdfUrl;

    private UserPublicProfileResponse owner;

    private LocalDate grantAt;
}
