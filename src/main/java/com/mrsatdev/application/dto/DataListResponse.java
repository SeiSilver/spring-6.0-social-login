package com.mrsatdev.application.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import com.mrsatdev.application.dto.common.MetaDataResponse;
import lombok.*;
import org.springframework.http.HttpStatus;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static com.mrsatdev.domain.constant.DataResponseConstant.TIME_PATTERN;
import static com.mrsatdev.domain.constant.DataResponseConstant.TIMEZONE;

@Value
@Builder
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class DataListResponse implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    @JsonProperty("timeStamp")
    @JsonFormat(shape = JsonFormat.Shape.STRING,
            pattern = TIME_PATTERN,
            timezone = TIMEZONE)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    LocalDateTime timeStamp;

    @JsonProperty("httpStatus")
    HttpStatus httpStatus;

    @JsonProperty("data")
    List<?> data;

    @JsonProperty("metaData")
    MetaDataResponse metaData;

    @JsonProperty("message")
    String message;
}

