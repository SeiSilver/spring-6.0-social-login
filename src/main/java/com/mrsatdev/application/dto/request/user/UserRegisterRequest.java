package com.mrsatdev.application.dto.request.user;

import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.web.multipart.MultipartFile;

import java.time.LocalDate;

import static com.mrsatdev.domain.constant.ValidatorPattern.*;
import static com.mrsatdev.domain.constant.error.InputErrorConstant.*;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserRegisterRequest {

    @NotNull(message = NOT_BLANK_PATTERN)
    @Pattern(regexp = USERNAME_PATTERN,
            message = BAD_USERNAME)
    private String username;

    @NotNull(message = NOT_BLANK_PATTERN)
    @Pattern(regexp = EMAIL_PATTERN,
            message = BAD_EMAIL)
    private String email;

    @NotNull(message = NOT_BLANK_PATTERN)
    @Pattern(regexp = NORMAL_NAME_PATTERN,
            message = BAD_NAME)
    private String fullName;

    @Pattern(regexp = SHORT_TEXT_PATTERN,
            message = BAD_TEXT)
    private String mainTitle;

    @NotNull(message = NOT_BLANK_PATTERN)
    @Pattern(regexp = PASSWORD_PATTERN,
            message = BAD_PASSWORD)
    private String password;

    @Pattern(regexp = PHONE_NUMBER_PATTERN,
            message = BAD_PHONE)
    private String phone;

    @Pattern(regexp = MEDIUM_TEXT_PATTERN,
            message = BAD_TEXT)
    private String bio;

    private LocalDate birthday;

    private MultipartFile avatar;

    private String oauthToken;
}
