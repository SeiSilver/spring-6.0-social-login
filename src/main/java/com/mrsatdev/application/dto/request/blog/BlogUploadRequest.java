package com.mrsatdev.application.dto.request.blog;

import com.mrsatdev.infrastructure.entity.enumeration.BlogType;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

import static com.mrsatdev.domain.constant.ValidatorPattern.*;
import static com.mrsatdev.domain.constant.error.InputErrorConstant.BAD_TEXT;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BlogUploadRequest {

    @NotNull(message = NOT_BLANK_PATTERN)
    @Pattern(regexp = MEDIUM_TEXT_PATTERN,
            message = BAD_TEXT)
    private String friendlyName;

    @NotNull(message = NOT_BLANK_PATTERN)
    @Pattern(regexp = MEDIUM_TEXT_PATTERN,
            message = BAD_TEXT)
    private String title;

    @Pattern(regexp = MEDIUM_TEXT_PATTERN,
            message = BAD_TEXT)
    private String description;

    @NotNull(message = NOT_BLANK_PATTERN)
    private MultipartFile picture;

    @NotNull(message = NOT_BLANK_PATTERN)
    private BlogType type;

    @NotNull(message = NOT_BLANK_PATTERN)
    private List<Long> categories;

    @NotNull(message = NOT_BLANK_PATTERN)
    @Pattern(regexp = LONG_TEXT_PATTERN,
            message = BAD_TEXT)
    private String html;

    @Pattern(regexp = MEDIUM_TEXT_PATTERN,
            message = BAD_TEXT)
    private String videoUrl;
}
