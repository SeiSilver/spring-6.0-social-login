package com.mrsatdev.application.dto.request.profile_cert;

import com.mrsatdev.infrastructure.entity.enumeration.ProfileCertType;
import jakarta.validation.constraints.Pattern;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.web.multipart.MultipartFile;

import static com.mrsatdev.domain.constant.ValidatorPattern.*;
import static com.mrsatdev.domain.constant.error.InputErrorConstant.BAD_TEXT;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ProfileCertUpdateRequest {

    private ProfileCertType type;

    @Pattern(regexp = SHORT_TEXT_PATTERN,
            message = BAD_TEXT)
    private String code;

    @Pattern(regexp = SHORT_TEXT_PATTERN,
            message = BAD_TEXT)
    private String organization;

    @Pattern(regexp = SHORT_TEXT_PATTERN,
            message = BAD_TEXT)
    private String name;

    @Pattern(regexp = LONG_TEXT_PATTERN + "|" + BLANK_PATTERN,
            message = BAD_TEXT)
    private String detail;

    private MultipartFile picture;

    private MultipartFile pdf;
}
