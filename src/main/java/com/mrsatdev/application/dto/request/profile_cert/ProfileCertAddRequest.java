package com.mrsatdev.application.dto.request.profile_cert;

import com.mrsatdev.infrastructure.entity.enumeration.ProfileCertType;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.web.multipart.MultipartFile;

import java.time.LocalDate;

import static com.mrsatdev.domain.constant.ValidatorPattern.*;
import static com.mrsatdev.domain.constant.error.InputErrorConstant.BAD_TEXT;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ProfileCertAddRequest {

    @NotNull(message = NOT_BLANK_PATTERN)
    private ProfileCertType type;

    @NotNull(message = NOT_BLANK_PATTERN)
    @Pattern(regexp = SHORT_TEXT_PATTERN,
            message = BAD_TEXT)
    private String code;

    @NotNull(message = NOT_BLANK_PATTERN)
    @Pattern(regexp = SHORT_TEXT_PATTERN,
            message = BAD_TEXT)
    private String organization;

    @NotNull(message = NOT_BLANK_PATTERN)
    @Pattern(regexp = SHORT_TEXT_PATTERN,
            message = BAD_TEXT)
    private String name;

    @Pattern(regexp = LONG_TEXT_PATTERN,
            message = BAD_TEXT)
    private String detail;

    @NotNull(message = NOT_BLANK_PATTERN)
    private MultipartFile picture;

    private MultipartFile pdf;

    private LocalDate grantAt;
}
