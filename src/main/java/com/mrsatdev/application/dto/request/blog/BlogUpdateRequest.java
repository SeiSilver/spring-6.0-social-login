package com.mrsatdev.application.dto.request.blog;

import com.mrsatdev.infrastructure.entity.model.BlogCategory;
import jakarta.validation.constraints.Pattern;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

import static com.mrsatdev.domain.constant.ValidatorPattern.LONG_TEXT_PATTERN;
import static com.mrsatdev.domain.constant.ValidatorPattern.MEDIUM_TEXT_PATTERN;
import static com.mrsatdev.domain.constant.error.InputErrorConstant.BAD_TEXT;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BlogUpdateRequest {

    @Pattern(regexp = MEDIUM_TEXT_PATTERN,
            message = BAD_TEXT)
    private String friendlyName;

    @Pattern(regexp = MEDIUM_TEXT_PATTERN,
            message = BAD_TEXT)
    private String title;

    @Pattern(regexp = MEDIUM_TEXT_PATTERN,
            message = BAD_TEXT)
    private String description;

    private MultipartFile pictureUrl;

    private List<BlogCategory> categories;

    @Pattern(regexp = LONG_TEXT_PATTERN,
            message = BAD_TEXT)
    private String html;

    @Pattern(regexp = MEDIUM_TEXT_PATTERN,
            message = BAD_TEXT)
    private String videoUrl;
}
