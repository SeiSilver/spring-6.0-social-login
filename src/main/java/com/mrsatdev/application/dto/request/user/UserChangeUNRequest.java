package com.mrsatdev.application.dto.request.user;

import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import static com.mrsatdev.domain.constant.ValidatorPattern.NOT_BLANK_PATTERN;
import static com.mrsatdev.domain.constant.ValidatorPattern.USERNAME_PATTERN;
import static com.mrsatdev.domain.constant.error.InputErrorConstant.BAD_USERNAME;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserChangeUNRequest {

    @NotNull(message = NOT_BLANK_PATTERN)
    @Pattern(regexp = USERNAME_PATTERN,
            message = BAD_USERNAME)
    private String username;
}
