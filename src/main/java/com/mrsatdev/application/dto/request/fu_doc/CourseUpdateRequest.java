package com.mrsatdev.application.dto.request.fu_doc;

import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CourseUpdateRequest {

    @NotNull
    private Long courseId;

    @NotNull
    private Long documentId;

    @NotNull
    private boolean isAdded;
}
