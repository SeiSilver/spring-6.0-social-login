package com.mrsatdev.application.dto.request.user;

import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import static com.mrsatdev.domain.constant.ValidatorPattern.NOT_BLANK_PATTERN;
import static com.mrsatdev.domain.constant.ValidatorPattern.PASSWORD_PATTERN;
import static com.mrsatdev.domain.constant.error.InputErrorConstant.BAD_PASSWORD;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserChangePasswordRequest {

    @NotNull(message = NOT_BLANK_PATTERN)
    private String currentPassword;

    @NotNull(message = NOT_BLANK_PATTERN)
    @Pattern(regexp = PASSWORD_PATTERN, message = BAD_PASSWORD)
    private String newPassword;
}
