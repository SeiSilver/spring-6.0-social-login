package com.mrsatdev.application.dto.request.contact;

import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.web.multipart.MultipartFile;

import static com.mrsatdev.domain.constant.ValidatorPattern.MEDIUM_TEXT_PATTERN;
import static com.mrsatdev.domain.constant.ValidatorPattern.NOT_BLANK_PATTERN;
import static com.mrsatdev.domain.constant.error.InputErrorConstant.BAD_TEXT;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ContactSubmitRequest {

    @NotNull(message = NOT_BLANK_PATTERN)
    @Pattern(regexp = MEDIUM_TEXT_PATTERN,
            message = BAD_TEXT)
    private String title;

    @NotNull(message = NOT_BLANK_PATTERN)
    @Pattern(regexp = MEDIUM_TEXT_PATTERN,
            message = BAD_TEXT)
    private String detail;

    @NotNull(message = NOT_BLANK_PATTERN)
    @Pattern(regexp = MEDIUM_TEXT_PATTERN,
            message = BAD_TEXT)
    private String contact_link;

    @NotNull(message = NOT_BLANK_PATTERN)
    private MultipartFile attachment;
}
