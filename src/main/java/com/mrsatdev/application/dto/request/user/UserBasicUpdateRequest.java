package com.mrsatdev.application.dto.request.user;

import jakarta.validation.constraints.Pattern;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.web.multipart.MultipartFile;

import java.time.LocalDate;

import static com.mrsatdev.domain.constant.ValidatorPattern.*;
import static com.mrsatdev.domain.constant.error.InputErrorConstant.*;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserBasicUpdateRequest {

    @Pattern(regexp = NORMAL_NAME_PATTERN,
            message = BAD_NAME)
    private String fullName;

    @Pattern(regexp = SHORT_TEXT_PATTERN + "|" + BLANK_PATTERN,
            message = BAD_TEXT)
    private String mainTitle;

    @Pattern(regexp = PHONE_NUMBER_PATTERN + "|" + BLANK_PATTERN,
            message = BAD_PHONE)
    private String phone;

    @Pattern(regexp = MEDIUM_TEXT_PATTERN + "|" + BLANK_PATTERN,
            message = BAD_TEXT)
    private String bio;

    private LocalDate birthday;

    private MultipartFile avatar;
}
