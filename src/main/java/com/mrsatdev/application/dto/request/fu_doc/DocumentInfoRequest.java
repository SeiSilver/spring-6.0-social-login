package com.mrsatdev.application.dto.request.fu_doc;

import jakarta.validation.constraints.Pattern;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.web.multipart.MultipartFile;

import static com.mrsatdev.domain.constant.ValidatorPattern.MEDIUM_TEXT_PATTERN;
import static com.mrsatdev.domain.constant.ValidatorPattern.SHORT_TEXT_PATTERN;
import static com.mrsatdev.domain.constant.error.InputErrorConstant.BAD_TEXT;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DocumentInfoRequest {

    @Pattern(regexp = SHORT_TEXT_PATTERN,
            message = BAD_TEXT)
    private String title;

    @Pattern(regexp = MEDIUM_TEXT_PATTERN,
            message = BAD_TEXT)
    private String description;

    private MultipartFile picture;

    @Pattern(regexp = MEDIUM_TEXT_PATTERN,
            message = BAD_TEXT)
    private String link;

    @Pattern(regexp = SHORT_TEXT_PATTERN,
            message = BAD_TEXT)
    private String code;

    @Pattern(regexp = MEDIUM_TEXT_PATTERN,
            message = BAD_TEXT)
    private String changeDescription;
}
