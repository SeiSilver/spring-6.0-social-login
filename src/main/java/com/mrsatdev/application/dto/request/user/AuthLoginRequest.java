package com.mrsatdev.application.dto.request.user;

import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import static com.mrsatdev.domain.constant.ValidatorPattern.NOT_BLANK_PATTERN;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AuthLoginRequest {

    @NotNull(message = NOT_BLANK_PATTERN)
    private String identifier;

    @NotNull(message = NOT_BLANK_PATTERN)
    private String password;
}