package com.mrsatdev.application.controller;

import com.mrsatdev.application.dto.GenericResponse;
import com.mrsatdev.domain.helper.HttpHelper;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.web.csrf.CsrfToken;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
@RequiredArgsConstructor
public class GlobalController {

    private static final Logger logger = LoggerFactory.getLogger(GlobalController.class);

    @GetMapping("/healthcheck")
    public ResponseEntity<GenericResponse> healthcheck() {
        logger.info("_healthcheck");
        return HttpHelper.createGenericOKResponse(HttpStatus.OK, "healthy");
    }

    @GetMapping("/csrf-token")
    public CsrfToken csrf(CsrfToken token) {
        logger.info("_csrf");
        return token;
    }
}
