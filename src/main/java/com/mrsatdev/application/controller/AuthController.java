package com.mrsatdev.application.controller;

import com.mrsatdev.application.dto.DataResponse;
import com.mrsatdev.application.dto.GenericResponse;
import com.mrsatdev.application.dto.request.user.AuthLoginRequest;
import com.mrsatdev.application.dto.request.user.UserRegisterRequest;
import com.mrsatdev.application.dto.response.user.UserLoginResponse;
import com.mrsatdev.domain.helper.HttpHelper;
import com.mrsatdev.infrastructure.service.AuthService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static com.mrsatdev.domain.constant.DataResponseConstant.ACTION_OK;
import static com.mrsatdev.domain.constant.DataResponseConstant.GET_OK;

@RestController
@RequestMapping("/auth")
@RequiredArgsConstructor
public class AuthController {

    private final AuthService authService;

    private static final Logger logger = LoggerFactory.getLogger(AuthController.class);

    @PostMapping("/login")
    public ResponseEntity<DataResponse> login(@RequestBody @Valid AuthLoginRequest authLoginRequest) {
        logger.info("_login");
        return HttpHelper.createDataResponse(HttpStatus.OK, authService.login(authLoginRequest), ACTION_OK);
    }

    @PostMapping(value = "/register", consumes = {"multipart/form-data"})
    public ResponseEntity<GenericResponse> register(@ModelAttribute @Valid UserRegisterRequest userRegisterRequest) {
        logger.info("_register");
        authService.register(userRegisterRequest);
        return HttpHelper.createGenericOKResponse(HttpStatus.OK, ACTION_OK);
    }

    @GetMapping("/current-user")
    public ResponseEntity<DataResponse> getCurrentUser() {
        logger.info("_getCurrentUser");
        UserLoginResponse userLoginResponse = authService.getCurrentUser();
        return HttpHelper.createDataResponse(HttpStatus.OK, userLoginResponse, GET_OK);
    }
}
