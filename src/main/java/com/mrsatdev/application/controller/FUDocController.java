package com.mrsatdev.application.controller;

import com.mrsatdev.application.dto.DataListResponse;
import com.mrsatdev.application.dto.DataResponse;
import com.mrsatdev.domain.helper.HttpHelper;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static com.mrsatdev.domain.constant.DataResponseConstant.GET_OK;

@RestController
@RequestMapping("/fudoc")
@RequiredArgsConstructor
public class FUDocController {

    private static final Logger logger = LoggerFactory.getLogger(FUDocController.class);

    @GetMapping("/courses")
    public ResponseEntity<DataListResponse> getAllCourse() {
        logger.info("_getAllCourse");
        return HttpHelper.createDataListResponse(HttpStatus.OK, null, null, GET_OK);
    }

    @GetMapping("/documents")
    public ResponseEntity<DataListResponse> getAllDocument(@PageableDefault(size = 25) Pageable pageable) {
        logger.info("_getAllDocument");
        return HttpHelper.createDataListResponse(HttpStatus.OK, null, null, GET_OK);
    }

    @GetMapping("/course/{id}")
    public ResponseEntity<DataListResponse> getAllDocumentByCourse(@PathVariable Long id, @PageableDefault(size = 25) Pageable pageable) {
        logger.info("_getAllDocumentByCourse");
        return HttpHelper.createDataListResponse(HttpStatus.OK, null, null, GET_OK);
    }

    @GetMapping("/document/{id}")
    public ResponseEntity<DataResponse> getDocument(@PathVariable Long id) {
        logger.info("_getDocument");
        return HttpHelper.createDataResponse(HttpStatus.OK, null, GET_OK);
    }
}
