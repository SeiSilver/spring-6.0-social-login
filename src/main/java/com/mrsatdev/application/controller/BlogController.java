package com.mrsatdev.application.controller;

import com.mrsatdev.application.dto.DataListResponse;
import com.mrsatdev.application.dto.DataResponse;
import com.mrsatdev.application.dto.GenericResponse;
import com.mrsatdev.application.dto.request.blog.BlogUpdateRequest;
import com.mrsatdev.application.dto.request.blog.BlogUploadRequest;
import com.mrsatdev.domain.helper.HttpHelper;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import static com.mrsatdev.domain.constant.DataResponseConstant.ACTION_OK;
import static com.mrsatdev.domain.constant.DataResponseConstant.GET_OK;

@RestController
@RequestMapping("/blog")
@RequiredArgsConstructor
public class BlogController {

    private static final Logger logger = LoggerFactory.getLogger(BlogController.class);

    @GetMapping("/admin/list")
    @PreAuthorize("hasAnyAuthority('FULL_ACCESS')")
    public ResponseEntity<DataListResponse> getAllBlogAdmin(@PageableDefault(size = 25) Pageable pageable) {
        logger.info("_getAllBlogAdmin");
        return HttpHelper.createDataListResponse(HttpStatus.OK, null, null, GET_OK);
    }

    @GetMapping("/admin/await")
    @PreAuthorize("hasAnyAuthority('FULL_ACCESS', 'PUBLISH_BLOG')")
    public ResponseEntity<DataListResponse> getAllBlogAwaitingAdmin(@PageableDefault(size = 25) Pageable pageable) {
        logger.info("_getAllBlogAwaitingAdmin");
        return HttpHelper.createDataListResponse(HttpStatus.OK, null, null, GET_OK);
    }

    @GetMapping("/managed/list")
    @PreAuthorize("hasAnyAuthority('FULL_ACCESS', 'MANAGE_OWN')")
    public ResponseEntity<DataListResponse> getAllManagedBlog(@PageableDefault(size = 25) Pageable pageable) {
        logger.info("_getAllManagedBlog");
        return HttpHelper.createDataListResponse(HttpStatus.OK, null, null, GET_OK);
    }

    @GetMapping("/managed/{id}")
    @PreAuthorize("hasAnyAuthority('FULL_ACCESS') " +
            "|| (hasAnyAuthority('MANAGE_OWN') && @preAuthenticationVerifyService.verifyBlogOwner(#id))")
    public ResponseEntity<DataResponse> getManagedBlog(@PathVariable Long id) {
        logger.info("_getManagedBlog");
        return HttpHelper.createDataResponse(HttpStatus.OK, null, GET_OK);
    }

    @GetMapping("/top")
    public ResponseEntity<DataListResponse> getTopPageBlog() {
        logger.info("_getTopPageBlog");
        return HttpHelper.createDataListResponse(HttpStatus.OK, null, null, GET_OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<DataResponse> getBlog(@PathVariable Long id) {
        logger.info("_getBlog");
        return HttpHelper.createDataResponse(HttpStatus.OK, null, GET_OK);
    }

    @GetMapping("/user/{id}")
    public ResponseEntity<DataListResponse> getAllPublicBlogByUser(@PathVariable Long id, @PageableDefault(size = 25) Pageable pageable) {
        logger.info("_getAllPublicBlogByUser");
        return HttpHelper.createDataListResponse(HttpStatus.OK, null, null, GET_OK);
    }

    @PostMapping("/add")
    @PreAuthorize("hasAnyAuthority('FULL_CONTROL', 'SUBMIT_CONTENT')")
    public ResponseEntity<GenericResponse> addBlog(@ModelAttribute @Valid BlogUploadRequest blogUploadRequest) {
        logger.info("_addBlog");
        return HttpHelper.createGenericOKResponse(HttpStatus.OK, ACTION_OK);
    }

    @PutMapping("/edit/{id}")
    @PreAuthorize("hasAnyAuthority('FULL_CONTROL')" +
            "|| (hasAnyAuthority('MANAGE_OWN') && @preAuthenticationVerifyService.verifyBlogOwner(#id))")
    public ResponseEntity<GenericResponse> editBlog(@PathVariable Long id, @ModelAttribute @Valid BlogUpdateRequest blogUpdateRequest) {
        logger.info("_editBlog");
        return HttpHelper.createGenericOKResponse(HttpStatus.OK, ACTION_OK);
    }

    @PutMapping("/pub/{id}")
    @PreAuthorize("hasAnyAuthority('FULL_CONTROL', 'PUBLISH_BLOG')" +
            "|| (hasAnyAuthority('MANAGE_OWN') && @preAuthenticationVerifyService.verifyBlogOwner(#id))")
    public ResponseEntity<GenericResponse> pubBlog(@PathVariable Long id) {
        logger.info("_pubBlog");
        return HttpHelper.createGenericOKResponse(HttpStatus.OK, ACTION_OK);
    }

    @PutMapping("/down/{id}")
    @PreAuthorize("hasAnyAuthority('FULL_CONTROL')")
    public ResponseEntity<GenericResponse> unPubBlog(@PathVariable Long id) {
        logger.info("_unPubBlog");
        return HttpHelper.createGenericOKResponse(HttpStatus.OK, ACTION_OK);
    }
}
