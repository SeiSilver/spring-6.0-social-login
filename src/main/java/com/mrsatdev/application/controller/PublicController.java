package com.mrsatdev.application.controller;

import com.mrsatdev.application.dto.GenericResponse;
import com.mrsatdev.domain.helper.HttpHelper;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/public")
@RequiredArgsConstructor
public class PublicController {

    private static final Logger logger = LoggerFactory.getLogger(PublicController.class);

    @GetMapping("/blog_top_page")
    public ResponseEntity<GenericResponse> getBlogTopPage() {
        logger.info("_getBlogTopPage");
        return HttpHelper.createGenericOKResponse(HttpStatus.OK, "{blog}");
    }
}
