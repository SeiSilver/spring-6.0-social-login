package com.mrsatdev.application.controller;

import com.mrsatdev.application.dto.DataListResponse;
import com.mrsatdev.application.dto.DataResponse;
import com.mrsatdev.application.dto.GenericResponse;
import com.mrsatdev.application.dto.request.user.UserBasicUpdateRequest;
import com.mrsatdev.domain.helper.HttpHelper;
import com.mrsatdev.infrastructure.entity.model.AuthUser;
import com.mrsatdev.infrastructure.service.UserService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import static com.mrsatdev.domain.constant.DataResponseConstant.ACTION_OK;
import static com.mrsatdev.domain.constant.DataResponseConstant.GET_OK;

@RestController
@RequestMapping("/user")
@RequiredArgsConstructor
public class UserController {

    private final UserService userService;

    private static final Logger logger = LoggerFactory.getLogger(UserController.class);

    @GetMapping("/managed/list")
    @PreAuthorize("hasAnyAuthority('FULL_ACCESS')")
    public ResponseEntity<DataListResponse> getAllManagedPaging(
            @RequestParam(required = false) String keyword,
            @PageableDefault(sort = {"id"}, size = 25) Pageable pageable) {
        logger.info("_getFullPaging");
        return userService.getAllManagedPaging(keyword, pageable);
    }

    @GetMapping("/managed/{id}")
    @PreAuthorize("hasAnyAuthority('FULL_ACCESS')")
    public ResponseEntity<DataResponse> getManagedById(@PathVariable Long id) {
        logger.info("_getFullById");
        AuthUser userResponse = userService.getManagedById(id);
        return HttpHelper.createDataResponse(HttpStatus.OK, userResponse, GET_OK);
    }

    //
    @PostMapping("/lock/{id}")
    @PreAuthorize("hasAnyAuthority('FULL_CONTROL')")
    public ResponseEntity<GenericResponse> lockAccount(@PathVariable Long id) {
        logger.info("_lockAccount");
        return HttpHelper.createGenericOKResponse(HttpStatus.OK, ACTION_OK);
    }

    //
    @PostMapping("/unlock/{id}")
    @PreAuthorize("hasAnyAuthority('FULL_CONTROL', 'UNBLOCK_USER')")
    public ResponseEntity<GenericResponse> unlockAccount(@PathVariable Long id) {
        logger.info("_unlockAccount");
        return HttpHelper.createGenericOKResponse(HttpStatus.OK, ACTION_OK);
    }

    //
    @GetMapping("/{id}")
    public ResponseEntity<DataResponse> getProfile(@PathVariable Long id) {
        logger.info("_getProfile");
        return HttpHelper.createDataResponse(HttpStatus.OK, null, GET_OK);
    }

    @PutMapping("/update/{id}")
    @PreAuthorize("hasAnyAuthority('FULL_CONTROL') " +
            "|| (hasAnyAuthority('MANAGE_OWN') && @preAuthenticationVerifyService.verifyOwner(#id))")
    public ResponseEntity<GenericResponse> update(@PathVariable Long id, @ModelAttribute @Valid UserBasicUpdateRequest userBasicUpdateRequest) {
        logger.info("_update");
        userService.update(id, userBasicUpdateRequest);
        return HttpHelper.createGenericOKResponse(HttpStatus.OK, ACTION_OK);
    }
}
