package com.mrsatdev.application.controller;

import com.mrsatdev.application.dto.DataListResponse;
import com.mrsatdev.application.dto.GenericResponse;
import com.mrsatdev.application.dto.request.profile_cert.ProfileCertAddRequest;
import com.mrsatdev.application.dto.request.profile_cert.ProfileCertUpdateRequest;
import com.mrsatdev.domain.helper.HttpHelper;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import static com.mrsatdev.domain.constant.DataResponseConstant.ACTION_OK;
import static com.mrsatdev.domain.constant.DataResponseConstant.GET_OK;

@RestController
@RequestMapping("/profile")
@RequiredArgsConstructor
public class ProfileController {

    private static final Logger logger = LoggerFactory.getLogger(ProfileController.class);

    @GetMapping("/cert/vip")
    public ResponseEntity<DataListResponse> getAllVIPCert() {
        logger.info("_getAllVIPCert");
        return HttpHelper.createDataListResponse(HttpStatus.OK, null, null, GET_OK);
    }

    @GetMapping("/cert/me")
    public ResponseEntity<DataListResponse> getAllCertPaginate(@PageableDefault(size = 25) Pageable pageable) {
        logger.info("_getAllCertPaginate");
        return HttpHelper.createDataListResponse(HttpStatus.OK, null, null, GET_OK);
    }

    @PostMapping("/cert/add")
    @PreAuthorize("hasAnyAuthority('FULL_CONTROL', 'MANAGE_OWN')")
    public ResponseEntity<GenericResponse> addCert(@ModelAttribute @Valid ProfileCertAddRequest profileCertAddRequest) {
        logger.info("_addCert");
        return HttpHelper.createGenericOKResponse(HttpStatus.OK, ACTION_OK);
    }

    @PutMapping("/cert/update/{id}")
    @PreAuthorize("hasAnyAuthority('FULL_CONTROL') " +
            "|| (hasAnyAuthority('MANAGE_OWN') && @preAuthenticationVerifyService.verifyCertOwner(#id))")
    public ResponseEntity<GenericResponse> updateCert(@PathVariable Long id, @ModelAttribute @Valid ProfileCertUpdateRequest profileCertUpdateRequest) {
        logger.info("_updateCert");
        return HttpHelper.createGenericOKResponse(HttpStatus.OK, ACTION_OK);
    }

    @DeleteMapping("/cert/delete/{id}")
    @PreAuthorize("hasAnyAuthority('FULL_CONTROL') " +
            "|| (hasAnyAuthority('MANAGE_OWN') && @preAuthenticationVerifyService.verifyCertOwner(#id))")
    public ResponseEntity<GenericResponse> deleteCert(@PathVariable Long id) {
        logger.info("_deleteCert");
        return HttpHelper.createGenericOKResponse(HttpStatus.OK, ACTION_OK);
    }
}
