package com.mrsatdev.application.controller;

import com.mrsatdev.application.dto.DataListResponse;
import com.mrsatdev.application.dto.DataResponse;
import com.mrsatdev.application.dto.GenericResponse;
import com.mrsatdev.application.dto.request.contact.ContactReplyRequest;
import com.mrsatdev.application.dto.request.contact.ContactSubmitRequest;
import com.mrsatdev.domain.helper.HttpHelper;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import static com.mrsatdev.domain.constant.DataResponseConstant.ACTION_OK;
import static com.mrsatdev.domain.constant.DataResponseConstant.GET_OK;

@RestController
@RequestMapping("/contact")
@RequiredArgsConstructor
public class ContactController {

    private static final Logger logger = LoggerFactory.getLogger(ContactController.class);

    @GetMapping("/admin/list")
    @PreAuthorize("hasAnyAuthority('FULL_ACCESS', 'REPLY_CONTACT')")
    public ResponseEntity<DataListResponse> getAllContactAdmin(@PageableDefault(size = 25) Pageable pageable) {
        logger.info("_getAllContactAdmin");
        return HttpHelper.createDataListResponse(HttpStatus.OK, null, null, GET_OK);
    }

    @GetMapping("/list")
    @PreAuthorize("hasAnyAuthority('FULL_ACCESS', 'SUBMIT_CONTENT')")
    public ResponseEntity<DataListResponse> getAllContactSent(@PageableDefault(size = 25) Pageable pageable) {
        logger.info("_getAllContactSent");
        return HttpHelper.createDataListResponse(HttpStatus.OK, null, null, GET_OK);
    }

    @GetMapping("/{id}")
    @PreAuthorize("hasAnyAuthority('FULL_ACCESS', 'SUBMIT_CONTENT')")
    public ResponseEntity<DataResponse> getContactDetail(@PathVariable Long id) {
        logger.info("_getContactDetail");
        return HttpHelper.createDataResponse(HttpStatus.OK, null, GET_OK);
    }

    @PostMapping("/add")
    @PreAuthorize("hasAnyAuthority('FULL_CONTROL', 'SUBMIT_CONTENT')")
    public ResponseEntity<GenericResponse> submitForm(@ModelAttribute @Valid ContactSubmitRequest contactSubmitRequest) {
        logger.info("_submitForm");
        return HttpHelper.createGenericOKResponse(HttpStatus.OK, ACTION_OK);
    }

    @PostMapping("/response/{id}")
    @PreAuthorize("hasAnyAuthority('FULL_CONTROL') " +
            "|| (hasAnyAuthority('MANAGE_OWN') && @preAuthenticationVerifyService.verifyContactFormOwner(#id))")
    public ResponseEntity<GenericResponse> submitReply(@PathVariable Long id, @ModelAttribute @Valid ContactReplyRequest contactReplyRequest) {
        logger.info("_submitReply");
        return HttpHelper.createGenericOKResponse(HttpStatus.OK, ACTION_OK);
    }

    @PostMapping("/admin/response/{id}")
    @PreAuthorize("hasAnyAuthority('FULL_CONTROL', 'REPLY_CONTACT')")
    public ResponseEntity<GenericResponse> submitResponse(@PathVariable Long id, @ModelAttribute @Valid ContactReplyRequest contactReplyRequest) {
        logger.info("_submitResponse");
        return HttpHelper.createGenericOKResponse(HttpStatus.OK, ACTION_OK);
    }
}
