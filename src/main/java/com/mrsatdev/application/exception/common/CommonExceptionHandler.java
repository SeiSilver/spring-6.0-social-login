package com.mrsatdev.application.exception.common;

import com.mrsatdev.application.dto.GenericResponse;
import com.mrsatdev.domain.constant.error.CommonErrorConstant;
import com.mrsatdev.domain.helper.HttpHelper;
import jakarta.persistence.NoResultException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.NoHandlerFoundException;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;

import static com.mrsatdev.domain.constant.error.CommonErrorConstant.*;
import static org.springframework.http.HttpStatus.METHOD_NOT_ALLOWED;
import static org.springframework.http.HttpStatus.NOT_FOUND;

@RestControllerAdvice
@Slf4j
public class CommonExceptionHandler {

    @ExceptionHandler(NoSuchElementException.class)
    public ResponseEntity<GenericResponse> noSuchElementException(NoSuchElementException exception) {
        log.error(exception.getMessage());
        return HttpHelper.createGenericErrorResponse(HttpStatus.BAD_REQUEST, exception.getMessage(), null);
    }

    @ExceptionHandler(NoResultException.class)
    public ResponseEntity<GenericResponse> notFoundException(NoResultException exception) {
        log.error(DATA_NOT_FOUND);
        return HttpHelper.createGenericErrorResponse(NOT_FOUND, DATA_NOT_FOUND, null);
    }

    @ExceptionHandler(NoHandlerFoundException.class)
    public ResponseEntity<GenericResponse> notFound404() {
        log.error(API_NOT_FOUND);
        return HttpHelper.createGenericErrorResponse(NOT_FOUND, API_NOT_FOUND, null);
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<GenericResponse> methodArgumentNotValidException(MethodArgumentNotValidException exception) {
        Map<String, String> errors = new HashMap<>();
        List<ObjectError> listException = exception.getAllErrors();
        try {
            for (ObjectError e : listException) {
                errors.put(((FieldError) e).getField(), e.getDefaultMessage());
            }
        } catch (Exception e) {
            log.error(e.getMessage());
            return HttpHelper.createGenericErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR, INTERNAL_SERVER_ERROR, errors);
        }
        log.error(BAD_REQUEST);
        return HttpHelper.createGenericErrorResponse(HttpStatus.BAD_REQUEST, BAD_REQUEST, errors);
    }

    @ExceptionHandler(HttpRequestMethodNotSupportedException.class)
    public ResponseEntity<GenericResponse> methodNotSupportedException(HttpRequestMethodNotSupportedException
                                                                               exception) {
        log.error(exception.getMessage());
        return HttpHelper.createGenericErrorResponse(METHOD_NOT_ALLOWED, METHOD_IS_NOT_ALLOWED, null);
    }

    @ExceptionHandler(IOException.class)
    public ResponseEntity<GenericResponse> ioException(IOException exception) {
        log.error(exception.getMessage());
        return HttpHelper.createGenericErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR, ERROR_PROCESSING_FILE, null);
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<GenericResponse> internalServerErrorException(Exception exception) {
        exception.printStackTrace();
        return HttpHelper.createGenericErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR, INTERNAL_SERVER_ERROR, null);
    }

    @ExceptionHandler(OtherCommonException.class)
    public ResponseEntity<GenericResponse> otherCommonException(OtherCommonException exception) {
        log.error(exception.getMsg());
        return HttpHelper.createGenericErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR, exception.getMsg(), null);
    }

    @ExceptionHandler(NotFoundException.class)
    public ResponseEntity<GenericResponse> notfoundException(NotFoundException exception) {
        return HttpHelper.createGenericErrorResponse(HttpStatus.NOT_FOUND, String.format(CommonErrorConstant.OBJECT_NOT_FOUND, exception.getObjectName()), null);
    }

    @ExceptionHandler(InputException.class)
    public ResponseEntity<GenericResponse> inputCommonException(InputException exception) {
        log.error(exception.getMsg());
        return HttpHelper.createGenericErrorResponse(HttpStatus.BAD_REQUEST, exception.getMsg(), null);
    }
}