package com.mrsatdev.application.exception.common;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public class OtherCommonException extends RuntimeException {
    private String msg;
}