package com.mrsatdev.application.exception.auth;

import com.auth0.jwt.exceptions.JWTDecodeException;
import com.auth0.jwt.exceptions.TokenExpiredException;
import com.mrsatdev.application.dto.GenericResponse;
import com.mrsatdev.domain.helper.HttpHelper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.LockedException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import static com.mrsatdev.domain.constant.error.AuthErrorConstant.*;
import static com.mrsatdev.domain.constant.error.SecurityErrorConstant.ACCESS_DENIED_MESSAGE;
import static org.springframework.http.HttpStatus.*;

@RestControllerAdvice
@Slf4j
public class AuthExceptionHandler {

    @ExceptionHandler(DisabledException.class)
    public ResponseEntity<GenericResponse> accountDisabledException() {
        log.error(ACCOUNT_LOCKED);
        return HttpHelper.createGenericErrorResponse(BAD_REQUEST, ACCOUNT_LOCKED, null);
    }

    @ExceptionHandler(BadCredentialsException.class)
    public ResponseEntity<GenericResponse> badCredentialsException() {
        log.error(INCORRECT_CREDENTIALS);
        return HttpHelper.createGenericErrorResponse(UNAUTHORIZED, INCORRECT_CREDENTIALS, null);
    }

    @ExceptionHandler(LockedException.class)
    public ResponseEntity<GenericResponse> lockedException() {
        log.error(ACCOUNT_LOCKED);
        return HttpHelper.createGenericErrorResponse(UNAUTHORIZED, ACCOUNT_LOCKED, null);
    }

    @ExceptionHandler(TokenExpiredException.class)
    public ResponseEntity<GenericResponse> tokenExpiredException(TokenExpiredException exception) {
        log.error(exception.getMessage());
        return HttpHelper.createGenericErrorResponse(UNAUTHORIZED, exception.getMessage(), null);
    }

    @ExceptionHandler(AccessDeniedException.class)
    public ResponseEntity<GenericResponse> accessDeniedException() {
        log.error(NOT_ENOUGH_PERMISSION);
        return HttpHelper.createGenericErrorResponse(FORBIDDEN, NOT_ENOUGH_PERMISSION, null);
    }

    @ExceptionHandler(JWTDecodeException.class)
    public ResponseEntity<GenericResponse> jwtDecodeException(JWTDecodeException exception) {
        log.error(exception.getMessage());
        return HttpHelper.createGenericErrorResponse(FORBIDDEN, ERROR_GRANT, null);
    }

    @ExceptionHandler(AuthBadRequestException.class)
    public ResponseEntity<GenericResponse> identityExistException(AuthBadRequestException identityExistException) {
        log.error(identityExistException.getMessage());
        return HttpHelper.createGenericErrorResponse(
                BAD_REQUEST,
                String.format(IDENTITY_ALREADY_EXISTS, identityExistException.getIdentity()),
                null
        );
    }

    @ExceptionHandler(SecurityFilterException.class)
    public ResponseEntity<GenericResponse> securityFilterException(SecurityFilterException securityFilterException) {
        log.error(securityFilterException.getMessage());
        return HttpHelper.createGenericErrorResponse(
                FORBIDDEN,
                ACCESS_DENIED_MESSAGE,
                null
        );
    }
}
