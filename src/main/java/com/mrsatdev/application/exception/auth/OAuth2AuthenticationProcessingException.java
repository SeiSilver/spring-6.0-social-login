package com.mrsatdev.application.exception.auth;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public class OAuth2AuthenticationProcessingException extends RuntimeException {
    private String msg;
}

