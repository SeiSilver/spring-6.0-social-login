package com.mrsatdev.application.exception.user;

import com.mrsatdev.application.dto.GenericResponse;
import com.mrsatdev.domain.helper.HttpHelper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import static org.springframework.http.HttpStatus.BAD_REQUEST;

@RestControllerAdvice
@Slf4j
public class UserExceptionHandler {

    @ExceptionHandler(UserNotFoundException.class)
    public ResponseEntity<GenericResponse> userNotFoundException(UserNotFoundException exception) {
        log.error(exception.getMessage());
        return HttpHelper.createGenericErrorResponse(BAD_REQUEST, exception.getMessage(), null);
    }
}
